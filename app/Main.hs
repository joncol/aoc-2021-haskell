import Control.Monad (forM_)
import System.FilePath.Posix ((</>))

import Text.Printf (printf)

import qualified Day1
import qualified Day2
import qualified Day3
import qualified Day4
import qualified Day5
import qualified Day6
import qualified Day7
import qualified Day8
import qualified Day9
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day17
import qualified Day18
import qualified Day19
import qualified Day20
import qualified Day21
import qualified Day22
import qualified Day23
import qualified Day24
import qualified Day25

dataDir :: FilePath
dataDir = "data"

main :: IO ()
main = do
  forM_ ((: []) . last $ zip [1 ..] solvers)
    $ \(i, (solvePart1, solvePart2)) -> do
        let filename = dataDir </> printf "input%02d.txt" (i :: Int)
        case solvePart1 of
          Just f  -> f filename >>= print
          Nothing -> pure ()
        case solvePart2 of
          Just f  -> f filename >>= print
          Nothing -> pure ()

solvers :: [(Maybe (FilePath -> IO Int), Maybe (FilePath -> IO Int))]
solvers =
  [ (Just Day1.part1 , Just Day1.part2)
  , (Just Day2.part1 , Just Day2.part2)
  , (Just Day3.part1 , Just Day3.part2)
  , (Just Day4.part1 , Just Day4.part2)
  , (Just Day5.part1 , Just Day5.part2)
  , (Just Day6.part1 , Just Day6.part2)
  , (Just Day7.part1 , Just Day7.part2)
  , (Just Day8.part1 , Just Day8.part2)
  , (Just Day9.part1 , Just Day9.part2)
  , (Just Day10.part1, Just Day10.part2)
  , (Just Day11.part1, Just Day11.part2)
  , (Just Day12.part1, Just Day12.part2)
  , (Just Day13.part1, Just Day13.part2)
  , (Just Day14.part1, Just Day14.part2)
  , (Just Day15.part1, Just Day15.part2)
  , (Just Day16.part1, Just Day16.part2)
  , (Just Day17.part1, Just Day17.part2)
  , (Just Day18.part1, Just Day18.part2)
  , (Just Day19.part1, Just Day19.part2)
  , (Just Day20.part1, Just Day20.part2)
  , (Just Day21.part1, Just Day21.part2)
  , (Just Day22.part1, Just Day22.part2)
  , (Just Day23.part1, Just Day23.part2)
  , (Just Day24.part1, Nothing)
  , (Just Day25.part1, Nothing)
  ]
