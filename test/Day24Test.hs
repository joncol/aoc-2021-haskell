module Day24Test where

-- import Control.DeepSeq (force)
-- import Control.Exception (evaluate)
import Control.Monad.State
import Data.Map ((!?))
import Optics
import Test.Hspec
import Text.PrettyPrint.Leijen (pretty)

import qualified Data.Cache.LRU as LRU
import qualified Data.Map as Map

import Day24
import Parser

resetALU' :: String -> ALU
resetALU' = flip resetALU (LRU.newLRU $ Just 1000)

spec_day24 :: Spec
spec_day24 = do
  describe "parseInstruction" $ do
    describe "inp" $ do
      it "parses an 'inp x' instruction" $ do
        instr <- parse parseInstruction "inp x"
        instr `shouldBe` Inp X

      it "parses an 'inp y' instruction" $ do
        instr <- parse parseInstruction "inp y"
        instr `shouldBe` Inp Y

      it "parses an 'inp z' instruction" $ do
        instr <- parse parseInstruction "inp z"
        instr `shouldBe` Inp Z

      it "parses an 'inp w' instruction" $ do
        instr <- parse parseInstruction "inp w"
        instr `shouldBe` Inp W

    describe "add" $ do
      it "parses an 'add x y' instruction" $ do
        instr <- parse parseInstruction "add x y"
        instr `shouldBe` Add X (Reg Y)

      it "parses an 'add w x' instruction" $ do
        instr <- parse parseInstruction "add w x"
        instr `shouldBe` Add W (Reg X)

      it "parses an 'add x 42' instruction" $ do
        instr <- parse parseInstruction "add w 42"
        instr `shouldBe` Add W (Val 42)

      it "parses an 'add x -123' instruction" $ do
        instr <- parse parseInstruction "add w -123"
        instr `shouldBe` Add W (Val (-123))

      it "parses an 'add x y' instruction" $ do
        instr <- parse parseInstruction "add x y"
        instr `shouldBe` Add X (Reg Y)

    describe "mul" $ do
      it "parses a 'mul x y' instruction" $ do
        instr <- parse parseInstruction "mul x y"
        instr `shouldBe` Mul X (Reg Y)

      it "parses a 'mul w x' instruction" $ do
        instr <- parse parseInstruction "mul w x"
        instr `shouldBe` Mul W (Reg X)

      it "parses a 'mul x 42' instruction" $ do
        instr <- parse parseInstruction "mul w 42"
        instr `shouldBe` Mul W (Val 42)

      it "parses a 'mul x -123' instruction" $ do
        instr <- parse parseInstruction "mul w -123"
        instr `shouldBe` Mul W (Val (-123))

      it "parses a 'mul x y' instruction" $ do
        instr <- parse parseInstruction "mul x y"
        instr `shouldBe` Mul X (Reg Y)

    describe "div" $ do
      it "parses a 'div x y' instruction" $ do
        instr <- parse parseInstruction "div x y"
        instr `shouldBe` Div X (Reg Y)

      it "parses a 'div w x' instruction" $ do
        instr <- parse parseInstruction "div w x"
        instr `shouldBe` Div W (Reg X)

      it "parses a 'div x 42' instruction" $ do
        instr <- parse parseInstruction "div w 42"
        instr `shouldBe` Div W (Val 42)

      it "parses a 'div x -123' instruction" $ do
        instr <- parse parseInstruction "div w -123"
        instr `shouldBe` Div W (Val (-123))

      it "parses a 'div x y' instruction" $ do
        instr <- parse parseInstruction "div x y"
        instr `shouldBe` Div X (Reg Y)

    describe "mod" $ do
      it "parses a 'mod x y' instruction" $ do
        instr <- parse parseInstruction "mod x y"
        instr `shouldBe` Mod X (Reg Y)

      it "parses a 'mod w x' instruction" $ do
        instr <- parse parseInstruction "mod w x"
        instr `shouldBe` Mod W (Reg X)

      it "parses a 'mod x 42' instruction" $ do
        instr <- parse parseInstruction "mod w 42"
        instr `shouldBe` Mod W (Val 42)

      it "parses a 'mod x y' instruction" $ do
        instr <- parse parseInstruction "mod x y"
        instr `shouldBe` Mod X (Reg Y)

    describe "eql" $ do
      it "parses a 'eql x y' instruction" $ do
        instr <- parse parseInstruction "eql x y"
        instr `shouldBe` Eql X (Reg Y)

      it "parses a 'eql w x' instruction" $ do
        instr <- parse parseInstruction "eql w x"
        instr `shouldBe` Eql W (Reg X)

      it "parses a 'eql x 42' instruction" $ do
        instr <- parse parseInstruction "eql w 42"
        instr `shouldBe` Eql W (Val 42)

      it "parses a 'eql x -123' instruction" $ do
        instr <- parse parseInstruction "eql w -123"
        instr `shouldBe` Eql W (Val (-123))

      it "parses a 'eql x y' instruction" $ do
        instr <- parse parseInstruction "eql x y"
        instr `shouldBe` Eql X (Reg Y)

  describe "runInstruction" $ do
    describe "inp" $ do
      it "runs an 'inp x' instruction" $ do
        let s = execState (runInstruction (Inp X)) (resetALU' "12345")
        (s ^. #regs) !? X `shouldBe` Just 1
        inp s `shouldBe` "2345"

      it "runs an 'inp y' instruction" $ do
        let s = execState (runInstruction (Inp Y)) (resetALU' "12345")
        (s ^. #regs) !? Y `shouldBe` Just 1
        inp s `shouldBe` "2345"

      it "takes only one element from the inp queue" $ do
        let s = execState (runInstruction (Inp W)) (resetALU' "123456")
        (s ^. #regs) !? W `shouldBe` Just 1
        inp s `shouldBe` "23456"

      -- it "throws when there are no inputs available" $ do
      --   evaluate (force (execState (runInstruction (Inp X)) (resetALU' "")))
      --     `shouldThrow` anyException

    describe "add" $ do
      it "runs an 'add x 1' instruction" $ do
        let s = execState (runInstruction (Add X (Val 1))) $ resetALU' ""
        (s ^. #regs) !? X `shouldBe` Just 1

      it "runs an 'add x y' instruction" $ do
        let s = execState (runInstruction (Add X (Reg Y)))
                          (resetALU' "" & #regs %~ Map.insert Y 10)
        (s ^. #regs) !? X `shouldBe` Just 10

      it "runs an 'add x y' instruction when 'x' is already set" $ do
        let s = execState
              (runInstruction (Add X (Reg Y)))
              (resetALU' "" & #regs %~ Map.insert X 6 . Map.insert Y 10)
        (s ^. #regs) !? X `shouldBe` Just 16

    describe "mul" $ do
      it "runs a 'mul x 2' instruction" $ do
        let s = execState (runInstruction (Mul X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 10)
        (s ^. #regs) !? X `shouldBe` Just 20

      it "runs a 'mul x y' instruction" $ do
        let s = execState
              (runInstruction (Mul X (Reg Y)))
              (resetALU' "" & #regs %~ Map.insert X 3 . Map.insert Y 10)
        (s ^. #regs) !? X `shouldBe` Just 30

    describe "div" $ do
      it "runs a 'div x 2' instruction" $ do
        let s = execState (runInstruction (Div X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 10)
        (s ^. #regs) !? X `shouldBe` Just 5

      it "truncates results" $ do
        let s = execState (runInstruction (Div X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 17)
        (s ^. #regs) !? X `shouldBe` Just 8

      it "runs a 'div x y' instruction" $ do
        let s = execState
              (runInstruction (Div X (Reg Y)))
              (resetALU' "" & #regs %~ Map.insert X 135 . Map.insert Y 11)
        (s ^. #regs) !? X `shouldBe` Just 12

      -- it "throws when the denominator is a literal zero" $ do
      --   evaluate
      --       (force
      --         (execState (runInstruction (Div X (Val 0)))
      --                    (resetALU' "" & #regs %~ Map.insert X 2)
      --         )
      --       )
      --     `shouldThrow` anyArithException

      -- it "throws when the denominator is a register with zero value" $ do
      --   evaluate
      --       (force
      --         (execState (runInstruction (Div X (Reg Y)))
      --                    (resetALU' "" & #regs %~ Map.insert X 2)
      --         )
      --       )
      --     `shouldThrow` anyArithException

    describe "mod" $ do
      it "runs a 'mod x 2' instruction for even 'x'" $ do
        let s = execState (runInstruction (Mod X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 10)
        (s ^. #regs) !? X `shouldBe` Just 0

      it "runs a 'mod x 2' instruction for odd 'x'" $ do
        let s = execState (runInstruction (Mod X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 9)
        (s ^. #regs) !? X `shouldBe` Just 1

      it "runs a 'mod x y' instruction for even 'x'" $ do
        let s = execState
              (runInstruction (Mod X (Reg Y)))
              (resetALU' "" & #regs %~ Map.insert X 134 . Map.insert Y 11)
        (s ^. #regs) !? X `shouldBe` Just 2

      it "runs a 'mod x y' instruction for odd 'x'" $ do
        let s = execState
              (runInstruction (Mod X (Reg Y)))
              (resetALU' "" & #regs %~ Map.insert X 135 . Map.insert Y 11)
        (s ^. #regs) !? X `shouldBe` Just 3

      -- it "throws when the second operand is <= 0" $ do
      --   evaluate
      --       (force
      --         (execState (runInstruction (Mod X (Val 0)))
      --                    (resetALU' "" & #regs %~ Map.insert X 2)
      --         )
      --       )
      --     `shouldThrow` anyArithException
      --   evaluate
      --       (force
      --         (execState (runInstruction (Mod X (Reg Y)))
      --                    (resetALU' "" & #regs %~ Map.insert X 2)
      --         )
      --       )
      --     `shouldThrow` anyArithException
      --   evaluate
      --       (force
      --         (execState (runInstruction (Mod X (Val (-1))))
      --                    (resetALU' "" & #regs %~ Map.insert X 2)
      --         )
      --       )
      --     `shouldThrow` anyArithException
      --   evaluate
      --       (force
      --         (execState
      --           (runInstruction (Mod X (Reg Y)))
      --           (resetALU' "" & #regs %~ Map.insert X 2 . Map.insert Y (-1))
      --         )
      --       )
      --     `shouldThrow` anyArithException


      -- it "throws when the first operand is negative" $ do
      --   evaluate
      --       (force
      --         (execState (runInstruction (Mod X (Val 2)))
      --                    (resetALU' "" & #regs %~ Map.insert X (-1))
      --         )
      --       )
      --     `shouldThrow` anyArithException
      --   evaluate
      --       (force
      --         (execState (runInstruction (Mod X (Val 3)))
      --                    (resetALU' "" & #regs %~ Map.insert X (-1))
      --         )
      --       )
      --     `shouldThrow` anyArithException

    describe "eql" $ do
      it "'eql x 2' sets x to 1 when 'x' is equal to 2" $ do
        let s = execState (runInstruction (Eql X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 2)
        (s ^. #regs) !? X `shouldBe` Just 1

      it "'eql x 2' sets x to 0 when 'x' is not equal to 2" $ do
        let s = execState (runInstruction (Eql X (Val 2)))
                          (resetALU' "" & #regs %~ Map.insert X 3)
        (s ^. #regs) !? X `shouldBe` Just 0

      it "'eql z w' sets z to 1 when 'z' is equal to 'w'" $ do
        let s = execState
              (runInstruction (Eql Z (Reg W)))
              (resetALU' "" & #regs %~ Map.insert Z 123 . Map.insert W 123)
        (s ^. #regs) !? Z `shouldBe` Just 1

      it "'eql z w' sets z to 0 when 'z' is not equal to 'w'" $ do
        let s = execState
              (runInstruction (Eql Z (Reg W)))
              (resetALU' "" & #regs %~ Map.insert Z 123 . Map.insert W 456)
        (s ^. #regs) !? Z `shouldBe` Just 0

  describe "runProgram" $ do
    it "runs a program to negate an input and store it in 'x'" $ do
      prog <- parse parseProgram "inp x\nmul x -1\n"
      let s = execState (runProgram prog) $ resetALU' "5"
      (s ^. #regs) !? X `shouldBe` Just (-5)

    it "caches the resulting state of the ALU for the previously consumed input"
      $ do
          prog <- parse parseProgram "inp x\nmul x -1\ninp y\n"
          let s = execState (runProgram prog) $ resetALU' "51"
          (s ^. #consumedInp) `shouldBe` "51"
          snd (LRU.lookup "5" (s ^. #cache)) `shouldBe` Just [-5, 0, 0, 0, 2]

    it "caches the resulting states of all but last previous inputs" $ do
      prog <- parse parseProgram "inp x\ninp y\ninp z\ninp w\n"
      let s = execState (runProgram prog) $ resetALU' "1234"
      (s ^. #consumedInp) `shouldBe` "1234"
      snd (LRU.lookup "1" (s ^. #cache)) `shouldBe` Just [1, 0, 0, 0, 1]
      snd (LRU.lookup "12" (s ^. #cache)) `shouldBe` Just [1, 2, 0, 0, 2]
      snd (LRU.lookup "123" (s ^. #cache)) `shouldBe` Just [1, 2, 3, 0, 3]
      snd (LRU.lookup "1234" (s ^. #cache)) `shouldBe` Nothing

    it "utilizes the cache for subsequent runs" $ do
      prog <- parse parseProgram "inp x\ninp y\ninp z\ninp w\n"
      let s = execState (runProgram prog) $ resetALU' "1234"
      (s ^. #consumedInp) `shouldBe` "1234"
      snd (LRU.lookup "1" (s ^. #cache)) `shouldBe` Just [1, 0, 0, 0, 1]
      snd (LRU.lookup "12" (s ^. #cache)) `shouldBe` Just [1, 2, 0, 0, 2]
      snd (LRU.lookup "123" (s ^. #cache)) `shouldBe` Just [1, 2, 3, 0, 3]
      snd (LRU.lookup "1234" (s ^. #cache)) `shouldBe` Nothing
      let s' = execState (runProgram prog) $ resetALU "1235" (s ^. #cache)
      (s' ^. #consumedInp) `shouldBe` "1235"
      snd (LRU.lookup "1" (s' ^. #cache)) `shouldBe` Just [1, 0, 0, 0, 1]
      snd (LRU.lookup "12" (s' ^. #cache)) `shouldBe` Just [1, 2, 0, 0, 2]
      snd (LRU.lookup "123" (s' ^. #cache)) `shouldBe` Just [1, 2, 3, 0, 3]
      snd (LRU.lookup "1235" (s' ^. #cache)) `shouldBe` Nothing

  describe "pretty for InstructionPP" $ do
    describe "inp" $ do
      it "pretty prints an `inp x` instruction" $ do
        show (pretty . InstructionPP $ Inp X)
          `shouldBe` "x = int(inp.popleft())"

      it "pretty prints an `inp y` instruction" $ do
        show (pretty . InstructionPP $ Inp Y)
          `shouldBe` "y = int(inp.popleft())"

    describe "add" $ do
      it "pretty prints an `add x 1` instruction" $ do
        show (pretty . InstructionPP $ Add X (Val 1)) `shouldBe` "x += 1"

      it "pretty prints an `add z w` instruction" $ do
        show (pretty . InstructionPP $ Add Z (Reg W)) `shouldBe` "z += w"

    describe "mul" $ do
      it "pretty prints an `mul x 1` instruction" $ do
        show (pretty . InstructionPP $ Mul X (Val 1)) `shouldBe` "x *= 1"

      it "pretty prints an `mul z w` instruction" $ do
        show (pretty . InstructionPP $ Mul Z (Reg W)) `shouldBe` "z *= w"

    describe "div" $ do
      it "pretty prints an `div x 2` instruction" $ do
        show (pretty . InstructionPP $ Div X (Val 2)) `shouldBe` "x //= 2"

      it "pretty prints an `div z w` instruction" $ do
        show (pretty . InstructionPP $ Div Z (Reg W)) `shouldBe` "z //= w"

    describe "mod" $ do
      it "pretty prints an `mod x 2` instruction" $ do
        show (pretty . InstructionPP $ Mod X (Val 2)) `shouldBe` "x %= 2"

      it "pretty prints an `mod z w` instruction" $ do
        show (pretty . InstructionPP $ Mod Z (Reg W)) `shouldBe` "z %= w"

    describe "eql" $ do
      it "pretty prints an `eql x 2` instruction" $ do
        show (pretty . InstructionPP $ Eql X (Val 2))
          `shouldBe` "x = 1 if x == 2 else 0"

      it "pretty prints an `eql z w` instruction" $ do
        show (pretty . InstructionPP $ Eql Z (Reg W))
          `shouldBe` "z = 1 if z == w else 0"
