module Day19Test where

import Data.List (elemIndex, nub, sort)
import Test.Hspec

import Day19
import Parser

s0 :: [Point]
s0 =
  [ Point 404    (-588) (-901)
  , Point 528    (-643) 409
  , Point (-838) 591    734
  , Point 390    (-675) (-793)
  , Point (-537) (-823) (-458)
  , Point (-485) (-357) 347
  , Point (-345) (-311) 381
  , Point (-661) (-816) (-575)
  , Point (-876) 649    763
  , Point (-618) (-824) (-621)
  , Point 553    345    (-567)
  , Point 474    580    667
  , Point (-447) (-329) 318
  , Point (-584) 868    (-557)
  , Point 544    (-627) (-890)
  , Point 564    392    (-477)
  , Point 455    729    728
  , Point (-892) 524    684
  , Point (-689) 845    (-530)
  , Point 423    (-701) 434
  , Point 7      (-33)  (-71)
  , Point 630    319    (-379)
  , Point 443    580    662
  , Point (-789) 900    (-551)
  , Point 459    (-707) 401
  ]

s1 :: [Point]
s1 =
  [ Point 686    422    578
  , Point 605    423    415
  , Point 515    917    (-361)
  , Point (-336) 658    858
  , Point 95     138    22
  , Point (-476) 619    847
  , Point (-340) (-569) (-846)
  , Point 567    (-361) 727
  , Point (-460) 603    (-452)
  , Point 669    (-402) 600
  , Point 729    430    532
  , Point (-500) (-761) 534
  , Point (-322) 571    750
  , Point (-466) (-666) (-811)
  , Point (-429) (-592) 574
  , Point (-355) 545    (-477)
  , Point 703    (-491) (-529)
  , Point (-328) (-685) 520
  , Point 413    935    (-424)
  , Point (-391) 539    (-444)
  , Point 586    (-435) 557
  , Point (-364) (-763) (-893)
  , Point 807    (-499) (-711)
  , Point 755    (-354) (-619)
  , Point 553    889    (-390)
  ]

s2 :: [Point]
s2 =
  [ Point 649    640    665
  , Point 682    (-795) 504
  , Point (-784) 533    (-524)
  , Point (-644) 584    (-595)
  , Point (-588) (-843) 648
  , Point (-30)  6      44
  , Point (-674) 560    763
  , Point 500    723    (-460)
  , Point 609    671    (-379)
  , Point (-555) (-800) 653
  , Point (-675) (-892) (-343)
  , Point 697    (-426) (-610)
  , Point 578    704    681
  , Point 493    664    (-388)
  , Point (-671) (-858) 530
  , Point (-667) 343    800
  , Point 571    (-461) (-707)
  , Point (-138) (-166) 112
  , Point (-889) 563    (-600)
  , Point 646    (-828) 498
  , Point 640    759    510
  , Point (-630) 509    768
  , Point (-681) (-892) (-333)
  , Point 673    (-379) (-804)
  , Point (-742) (-814) (-386)
  , Point 577    (-820) 562
  ]

s3 :: [Point]
s3 =
  [ Point (-589) 542    597
  , Point 605    (-692) 669
  , Point (-500) 565    (-823)
  , Point (-660) 373    557
  , Point (-458) (-679) (-417)
  , Point (-488) 449    543
  , Point (-626) 468    (-788)
  , Point 338    (-750) (-386)
  , Point 528    (-832) (-391)
  , Point 562    (-778) 733
  , Point (-938) (-730) 414
  , Point 543    643    (-506)
  , Point (-524) 371    (-870)
  , Point 407    773    750
  , Point (-104) 29     83
  , Point 378    (-903) (-323)
  , Point (-778) (-728) 485
  , Point 426    699    580
  , Point (-438) (-605) (-362)
  , Point (-469) (-447) (-387)
  , Point 509    732    623
  , Point 647    635    (-688)
  , Point (-868) (-804) 481
  , Point 614    (-800) 639
  , Point 595    780    (-596)
  ]

s4 :: [Point]
s4 =
  [ Point 727    592    562
  , Point (-293) (-554) 779
  , Point 441    611    (-461)
  , Point (-714) 465    (-776)
  , Point (-743) 427    (-804)
  , Point (-660) (-479) (-426)
  , Point 832    (-632) 460
  , Point 927    (-485) (-438)
  , Point 408    393    (-506)
  , Point 466    436    (-512)
  , Point 110    16     151
  , Point (-258) (-428) 682
  , Point (-393) 719    612
  , Point (-211) (-452) 876
  , Point 808    (-476) (-593)
  , Point (-575) 615    604
  , Point (-485) 667    467
  , Point (-680) 325    (-822)
  , Point (-627) (-443) (-432)
  , Point 872    (-547) (-609)
  , Point 833    512    582
  , Point 807    604    487
  , Point 839    (-516) 451
  , Point 891    (-625) 532
  , Point (-652) (-548) (-490)
  , Point 30     (-46)  (-14)
  ]

spec_day19 :: Spec
spec_day19 = do
  describe "rotFuns" $ do
    it "contains 24 rotation functions" $ do
      length rotFuns `shouldBe` 24
    it "produces unique results" $ do
      (length . nub . sort . map ($ Point 1 2 3) $ rotFuns) `shouldBe` 24
    it "the first item in the returned list is the passed argument" $ do
      head (map ($ Point 1 2 3) rotFuns) `shouldBe` Point 1 2 3

  describe "parseScanner" $ do
    let
      s
        = "--- scanner 0 ---\n\
          \404,-588,-901\n\
          \528,-643,409\n\
          \-838,591,734\n\
          \390,-675,-793\n\
          \-537,-823,-458\n\
          \-485,-357,347\n\
          \-345,-311,381\n\
          \-661,-816,-575\n\
          \-876,649,763\n\
          \-618,-824,-621\n\
          \553,345,-567\n\
          \474,580,667\n\
          \-447,-329,318\n\
          \-584,868,-557\n\
          \544,-627,-890\n\
          \564,392,-477\n\
          \455,729,728\n\
          \-892,524,684\n\
          \-689,845,-530\n\
          \423,-701,434\n\
          \7,-33,-71\n\
          \630,319,-379\n\
          \443,580,662\n\
          \-789,900,-551\n\
          \459,-707,401\n"
    it "parses all beacons of a scanner" $ do
      points <- parse parseScanner s
      length points `shouldBe` 25
    it "parses the coordinates of the beacons" $ do
      points <- parse parseScanner s
      head points `shouldBe` Point 404 (-588) (-901)
      last points `shouldBe` Point 459 (-707) 401

  describe "example beacon overlap" $ do
    let scanner0 =
          [ Point (-618) (-824) (-621)
          , Point (-537) (-823) (-458)
          , Point (-447) (-329) 318
          , Point 404    (-588) (-901)
          , Point 544    (-627) (-890)
          , Point 528    (-643) 409
          , Point (-661) (-816) (-575)
          , Point 390    (-675) (-793)
          , Point 423    (-701) 434
          , Point (-345) (-311) 381
          , Point 459    (-707) 401
          , Point (-485) (-357) 347
          ]

    let scanner1 =
          [ Point 686    422 578
          , Point 605    423 415
          , Point 515    917 (-361)
          , Point (-336) 658 858
          , Point (-476) 619 847
          , Point (-460) 603 (-452)
          , Point 729    430 532
          , Point (-322) 571 750
          , Point (-355) 545 (-477)
          , Point 413    935 (-424)
          , Point (-391) 539 (-444)
          , Point 553    889 (-390)
          ]

    let diff = Point 68 (-1246) (-43)

    let allDiffs =
          [ [ rp0 `minus` rp1 | rp0 <- rotations p0, rp1 <- rotations p1 ]
          | (p0, p1) <- scanner0 `zip` scanner1
          ]

    it "the length of the diff list is as expected" $ do
      length allDiffs `shouldBe` 12
      length (concat allDiffs) `shouldBe` 12 * 24 * 24

    it "the given offset should work for all 12 points" $ do
      all (diff `elem`) allDiffs `shouldBe` True

    it "the rotation config where the diff works is the same for all points"
      $ do
          (length . nub . sort . map (elemIndex diff) $ allDiffs) `shouldBe` 1

  -- describe "findMatch" $ do
  --   it "finds the offsets between scanners" $ do
  --     let (n01, d01) = fromJust $ findMatch s0 s1
  --     let f01        = rotFuns !! n01
  --     let s1'        = map f01 s1
  --     d01 `shouldBe` Point 68 (-1246) (-43)
  --     (snd <$> findMatch s0 s4) `shouldBe` Nothing

  --     let d04        = Point (-20) (-1133) 1061
  --     let (n14, d14) = fromJust $ findMatch s1' s4
  --     let f14        = rotFuns !! n14
  --     let s4'        = map f14 s4
  --     d01 `plus` d14 `shouldBe` d04

  --     let d03         = Point (-92) (-2380) (-20)
  --     let (_n13, d13) = fromJust $ findMatch s1' s3
  --     d01 `plus` d13 `shouldBe` d03

  --     let d02         = Point 1105 (-1205) 1229
  --     let (_n42, d42) = fromJust $ findMatch s4' s2
  --     d04 `plus` d42 `shouldBe` d02

  describe "findAllBeaconPositions" $ do
    let expectedBeacons =
          [ Point (-892) 524     684
          , Point (-876) 649     763
          , Point (-838) 591     734
          , Point (-789) 900     (-551)
          , Point (-739) (-1745) 668
          , Point (-706) (-3180) (-659)
          , Point (-697) (-3072) (-689)
          , Point (-689) 845     (-530)
          , Point (-687) (-1600) 576
          , Point (-661) (-816)  (-575)
          , Point (-654) (-3158) (-753)
          , Point (-635) (-1737) 486
          , Point (-631) (-672)  1502
          , Point (-624) (-1620) 1868
          , Point (-620) (-3212) 371
          , Point (-618) (-824)  (-621)
          , Point (-612) (-1695) 1788
          , Point (-601) (-1648) (-643)
          , Point (-584) 868     (-557)
          , Point (-537) (-823)  (-458)
          , Point (-532) (-1715) 1894
          , Point (-518) (-1681) (-600)
          , Point (-499) (-1607) (-770)
          , Point (-485) (-357)  347
          , Point (-470) (-3283) 303
          , Point (-456) (-621)  1527
          , Point (-447) (-329)  318
          , Point (-430) (-3130) 366
          , Point (-413) (-627)  1469
          , Point (-345) (-311)  381
          , Point (-36)  (-1284) 1171
          , Point (-27)  (-1108) (-65)
          , Point 7      (-33)   (-71)
          , Point 12     (-2351) (-103)
          , Point 26     (-1119) 1091
          , Point 346    (-2985) 342
          , Point 366    (-3059) 397
          , Point 377    (-2827) 367
          , Point 390    (-675)  (-793)
          , Point 396    (-1931) (-563)
          , Point 404    (-588)  (-901)
          , Point 408    (-1815) 803
          , Point 423    (-701)  434
          , Point 432    (-2009) 850
          , Point 443    580     662
          , Point 455    729     728
          , Point 456    (-540)  1869
          , Point 459    (-707)  401
          , Point 465    (-695)  1988
          , Point 474    580     667
          , Point 496    (-1584) 1900
          , Point 497    (-1838) (-617)
          , Point 527    (-524)  1933
          , Point 528    (-643)  409
          , Point 534    (-1912) 768
          , Point 544    (-627)  (-890)
          , Point 553    345     (-567)
          , Point 564    392     (-477)
          , Point 568    (-2007) (-577)
          , Point 605    (-1665) 1952
          , Point 612    (-1593) 1893
          , Point 630    319     (-379)
          , Point 686    (-3108) (-505)
          , Point 776    (-3184) (-501)
          , Point 846    (-3110) (-434)
          , Point 1135   (-1161) 1235
          , Point 1243   (-1093) 1063
          , Point 1660   (-552)  429
          , Point 1693   (-557)  386
          , Point 1735   (-437)  1738
          , Point 1749   (-1800) 1813
          , Point 1772   (-405)  1572
          , Point 1776   (-675)  371
          , Point 1779   (-442)  1789
          , Point 1780   (-1548) 337
          , Point 1786   (-1538) 337
          , Point 1847   (-1591) 415
          , Point 1889   (-1729) 1762
          , Point 1994   (-1805) 1792
          ]

    let (beacons, _) = foldScanners s0 [s1, s2, s3, s4] []

    it "ends with the correct number of beacons" $ do
      (length . nub . sort $ beacons) `shouldBe` 79

    it "finds the correct position of all beacons" $ do
      (nub . sort $ beacons) `shouldBe` sort expectedBeacons
