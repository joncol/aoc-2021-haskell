module Day22Test where

import Data.Text (Text)
import Test.Hspec
import Text.Megaparsec hiding (parse)

import Day22
import Parser

ex1 :: Text
ex1 =
  "on x=10..12,y=10..12,z=10..12\n\
  \on x=11..13,y=11..13,z=11..13\n\
  \off x=9..11,y=9..11,z=9..11\n\
  \on x=10..10,y=10..10,z=10..10\n"

spec_day22 :: Spec
spec_day22 = do
  describe "parseStep" $ do
    it "correctly parses input" $ do
      cubes <- parse (some . lexeme $ parseStep) ex1
      length cubes `shouldBe` 4
      cubes
        `shouldBe` [ (On , ((10, 12), (10, 12), (10, 12)))
                   , (On , ((11, 13), (11, 13), (11, 13)))
                   , (Off, ((9, 11), (9, 11), (9, 11)))
                   , (On , ((10, 10), (10, 10), (10, 10)))
                   ]

  describe "rangeOverlap" $ do
    it "calculates the intersection correctly for two non-intersecting ranges"
      $ do
          len (rangeOverlap (0, 5) (6, 10)) < 0 `shouldBe` True

    it "calculates the intersection correctly for two adjacent ranges" $ do
      len (rangeOverlap (0, 5) (5, 10)) `shouldBe` 0

    it "calculates the intersection correctly for two adjacent ranges" $ do
      rangeOverlap (0, 5) (3, 10) `shouldBe` (3, 5)

  describe "overlap" $ do
    it "calculates the overlap correctly when first list is empty" $ do
      overlap [] ((5, 9), (5, 9), (5, 9)) `shouldBe` 0

    it "calculates the overlap correctly between two disjoint cubes" $ do
      overlap [((10, 12), (10, 12), (10, 12))] ((5, 9), (5, 9), (5, 9))
        `shouldBe` 0

    it "calculates the overlap correctly when one cube is within another" $ do
      overlap [((10, 12), (10, 12), (10, 12))] ((9, 11), (9, 11), (9, 11))
        `shouldBe` 8
