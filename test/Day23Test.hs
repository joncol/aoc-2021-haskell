module Day23Test where

import Data.List ((\\), sort)
import Data.Map ((!?))
import Data.Text (Text)
import Test.Hspec

import Day23
import Parser

burrow1 :: Text
burrow1 =
  "#############\
  \#...........#\
  \###B#C#B#D###\
  \  #A#D#C#A#  \
  \  #########  "

burrow2 :: Text
burrow2 =
  "#############\
  \#...........#\
  \###D#.#.#.###\
  \  #.#.#.#.#  \
  \  #########  "

spec_day23 :: Spec
spec_day23 = do
  describe "parseBurrow" $ do
    it "correctly parses input" $ do
      burrow <- parse parseBurrow burrow1
      burrow
        `shouldBe` [ (Room 0, B)
                   , (Room 1, C)
                   , (Room 2, B)
                   , (Room 3, D)
                   , (Room 4, A)
                   , (Room 5, D)
                   , (Room 6, C)
                   , (Room 7, A)
                   ]

  describe "hallway" $ do
    it "contains the positions for the hallway" $ do
      hallway `shouldBe` map Hallway [0, 1, 3, 5, 7, 9, 10]

  describe "nearestHallwayCoord" $ do
    it "gets the nearest hallway position for second room" $ do
      nearestHallwayCoord (Room 5) `shouldBe` 4

  describe "canReachHallway" $ do
    it "returns `True` when amphipod is next to hallway"
      $          canReachHallway [(Room 0, D)] (Room 0)
      `shouldBe` True

    it "returns `True` when amphipod has clear passage to hallway"
      $          canReachHallway [(Room 4, D)] (Room 4)
      `shouldBe` True

    it "returns `False` when amphipod's passage to hallway is blocked"
      $          canReachHallway [(Room 0, D), (Room 4, D)] (Room 4)
      `shouldBe` False

  describe "isInFinalPosition" $ do
    it "returns `False` when amphipod is in hallway" $ do
      isInFinalPosition 2 [(Hallway 0, A)] (Hallway 0) `shouldBe` False

    it "returns `False` when lone amphipod is in wrong room" $ do
      isInFinalPosition 2 [(Room 0, B)] (Room 0) `shouldBe` False

    it "returns `True` when lone amphipod is in final position" $ do
      isInFinalPosition 2 [(Room 5, B)] (Room 5) `shouldBe` True

  describe "nextPositions" $ do
    it "gets the next positions for an amphipod in the first room" $ do
      nextPositions 2 [(Room 0, D)] (Room 0) `shouldBe` (hallway <> [Room 7])

    it "gets the next positions when another room is full" $ do
      nextPositions 2 [(Room 0, D), (Room 1, B), (Room 5, B)] (Room 0)
        `shouldBe` (hallway <> [Room 7])

    it "gets the next positions when another room is non-empty" $ do
      nextPositions 2 [(Room 0, D), (Room 5, B)] (Room 0)
        `shouldBe` (hallway <> [Room 7])

    it "gets the next positions when all other rooms are full" $ do
      nextPositions
          2
          [ (Room 0, D)
          , (Room 1, B)
          , (Room 2, D)
          , (Room 3, A)
          , (Room 5, B)
          , (Room 6, C)
          , (Room 7, D)
          ]
          (Room 0)
        `shouldBe` hallway

    it "gets the next positions for a D-amphipod in the first room" $ do
      nextPositions 2 [(Room 0, D)] (Room 0) `shouldBe` hallway <> [Room 7]

    it "gets the next positions for a C-amphipod in second room" $ do
      nextPositions 2 [(Room 5, C)] (Room 5) `shouldBe` hallway <> [Room 6]

    it "gets the next positions when hallway is non-empty" $ do
      nextPositions 2 [(Room 0, D), (Hallway 5, A)] (Room 0)
        `shouldBe` [Hallway 0, Hallway 1, Hallway 3]

    it "doesn't move first amphipod that is in final position" $ do
      nextPositions 2 [(Room 1, B), (Room 5, B)] (Room 1) `shouldBe` []

    it "doesn't move second amphipod that is in final position" $ do
      nextPositions 2 [(Room 1, B), (Room 5, B)] (Room 5) `shouldBe` []

    it
        "can move first amphipod that is in correct room, \
       \when another amphipod is blocked by it"
      $ do
          nextPositions 2 [(Room 1, B), (Room 5, A)] (Room 1) `shouldBe` hallway

    it "doesn't move amphipod within hallway, after first entering it" $ do
      nextPositions 2 [(Hallway 1, A), (Room 4, B)] (Hallway 1) `shouldBe` []

    it
        "doesn't move amphipod from hallway to a room, \
        \when there's another (incorrect) amphipod there"
      $ do
          nextPositions 2 [(Hallway 10, D), (Room 7, A)] (Hallway 10)
            `shouldBe` []

    it
        "doesn't move amphipod from room to a room, \
        \when there's another (incorrect) amphipod there"
      $ do
          nextPositions 2 [(Room 0, D), (Room 7, A)] (Room 0) `shouldBe` hallway

    it "moves from room to room, when possible" $ do
      nextPositions
          2
          [ (Hallway 0, D)
          , (Room 0   , B)
          , (Room 2   , C)
          , (Room 3   , D)
          , (Room 4   , A)
          , (Room 5   , B)
          , (Room 6   , C)
          , (Room 7   , A)
          ]
          (Room 0)
        `shouldBe` ((hallway \\ [Hallway 0]) <> [Room 1])

    it "finishes a position" $ do
      -- #############
      -- #DD........B#
      -- ###A#.#C#.###
      --   #A#B#C#.#
      --   #########
      let burrow =
            [ (Hallway 0 , D)
            , (Hallway 1 , D)
            , (Hallway 10, B)
            , (Room 0    , A)
            , (Room 2    , C)
            , (Room 4    , A)
            , (Room 5    , B)
            , (Room 6    , C)
            ]
      nextPositions 2 burrow (Hallway 0) `shouldBe` []
      nextPositions 2 burrow (Hallway 1) `shouldBe` [Room 7]
      nextPositions 2 burrow (Hallway 10) `shouldBe` [Room 1]

  describe "distance" $ do
    it "gets the distance between two hallway points" $ do
      distance (Hallway 1) (Hallway 3) `shouldBe` 2

    it "is independent of order" $ do
      distance (Hallway 3) (Hallway 1) `shouldBe` 2

    it "gets the distance between a room point and a hallway point 1" $ do
      distance (Room 0) (Hallway 0) `shouldBe` 3

    it "gets the distance between a room point and a hallway point 2" $ do
      distance (Room 7) (Hallway 3) `shouldBe` 7

    it "gets the distance between two room points" $ do
      distance (Room 0) (Room 7) `shouldBe` 9

  describe "floodFill" $ do
    it "fills a hallway blocked on both sides" $ do
      floodFill [3, 9] 6 `shouldBe` [4 .. 8]

    it "fills an empty hallway 1" $ do
      floodFill [] 6 `shouldBe` [0 .. 10]

    it "fills an empty hallway 2" $ do
      floodFill [] 2 `shouldBe` [0 .. 10]

    it "fills a hallway blocked on left side" $ do
      floodFill [1] 6 `shouldBe` [2 .. 10]

    it "fills a hallway blocked on right side" $ do
      floodFill [9] 6 `shouldBe` [0 .. 8]

    it "fills an empty hallway when on lower bound" $ do
      floodFill [] 0 `shouldBe` [0 .. 10]

    it "fills an empty hallway when on upper bound" $ do
      floodFill [] 10 `shouldBe` [0 .. 10]

  describe "floodFillLUT" $ do
    it "can be used to look up pre-calculated positions" $ do
      floodFillLUT !? ([], 0) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([], 1) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([], 2) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([], 3) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([], 4) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([], 5) `shouldBe` Just [0 .. 10]
      floodFillLUT !? ([3], 2) `shouldBe` Just [0 .. 2]
      floodFillLUT !? ([3], 6) `shouldBe` Just [4 .. 10]
      floodFillLUT !? ([1], 8) `shouldBe` Just [2 .. 10]
      floodFillLUT !? ([9], 6) `shouldBe` Just [0 .. 8]
      floodFillLUT !? ([3, 7], 6) `shouldBe` Just [4 .. 6]
      floodFillLUT !? ([3, 7], 8) `shouldBe` Just [8 .. 10]

  describe "adjacentEdges" $ do
    it "gets the adjacent edges of a simple position" $ do
      burrow <- parse parseBurrow burrow2
      burrow `shouldBe` [(Room 0, D)]
      adjacentEdges 2 burrow
        `shouldBe` [ Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 0, D)]
                          , weight = 3000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 1, D)]
                          , weight = 2000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 3, D)]
                          , weight = 2000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 5, D)]
                          , weight = 4000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 7, D)]
                          , weight = 6000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 9, D)]
                          , weight = 8000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Hallway 10, D)]
                          , weight = 9000
                          }
                   , Edge { from   = [(Room 0, D)]
                          , to     = [(Room 7, D)]
                          , weight = 9000
                          }
                   ]

    it "correctly weighs the edges" $ do
      let b =
            [ (Hallway 10, A)
            , (Room 0    , B)
            , (Room 2    , C)
            , (Room 3    , D)
            , (Room 4    , A)
            , (Room 5    , B)
            , (Room 6    , C)
            , (Room 7    , D)
            ]
      let r = (Room 0, B)
      sort (adjacentEdges 2 b) `shouldBe` sort
        [ Edge { from = b, to = moveAmphipod b r (Hallway 0, B), weight = 30 }
        , Edge { from = b, to = moveAmphipod b r (Hallway 1, B), weight = 20 }
        , Edge { from = b, to = moveAmphipod b r (Hallway 3, B), weight = 20 }
        , Edge { from = b, to = moveAmphipod b r (Hallway 5, B), weight = 40 }
        , Edge { from = b, to = moveAmphipod b r (Hallway 7, B), weight = 60 }
        , Edge { from = b, to = moveAmphipod b r (Hallway 9, B), weight = 80 }
        , Edge { from = b, to = moveAmphipod b r (Room 1, B), weight = 40 }
        ]

  describe "dijkstra" $ do
    it "gives the correct result for the example burrow" $ do
      burrow <- parse parseBurrow burrow1
      let (burrow', dists, _prev) =
            dijkstra burrow (adjacentEdges 2) (isBurrowDone 2)
      lookup burrow' dists `shouldBe` Just 12521
