module Day25Test where

import Data.Array
import Data.List (elemIndex, intercalate)
import Data.Maybe (isJust)
import Test.Hspec

import Day25

map1 :: [String]
map1 = ["..........", ".>v....v..", ".......>..", "..........  "]

map1' :: [String]
map1' = ["..........", ".>........", "..v....v>.", ".........."]

map2 :: [String]
map2 =
  [ "v...>>.vv>"
  , ".vv>>.vv.."
  , ">>.>v>...v"
  , ">>v>>.>.v."
  , "v>v.vv.v.."
  , ">.>>..v..."
  , ".vv..>.>v."
  , "v.v..>>v.v"
  , "....v..v.>"
  ]

spec_day25 :: Spec
spec_day25 = do
  describe "parseMap" $ do
    it "parses a map of sea cucumbers" $ do
      let m = parseSeafloor map1
      putStrLn $ showSeafloor m
      print $ elems m
      (length . filter isJust . elems $ m) `shouldBe` 4
      m ! (0, 0) `shouldBe` Nothing
      m ! (1, 1) `shouldBe` Just EastFacing
      m ! (2, 1) `shouldBe` Just SouthFacing
      m ! (7, 1) `shouldBe` Just SouthFacing
      m ! (7, 2) `shouldBe` Just EastFacing

  describe "step" $ do
    it "moves east-facing sea cucumbers correctly" $ do
      let m = parseSeafloor ["...>>>>>..."]
      showSeafloor (step m) `shouldBe` "...>>>>.>.."
      showSeafloor (step . step $ m) `shouldBe` "...>>>.>.>."

    it "moves east-facing, then south-facing sea cucumbers correctly" $ do
      let m = parseSeafloor map1
      showSeafloor (step m) `shouldBe` intercalate "\n" map1'

    it "stops moving after the expected number of steps for the example" $ do
      let m  = parseSeafloor map2
      let xs = iterate step m
      let zs = zipWith (==) xs (tail xs)
      (+ 1) <$> (True `elemIndex` zs) `shouldBe` Just 58
