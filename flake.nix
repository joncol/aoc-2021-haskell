{
  description = "aoc-2021-haskell";

  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix.url = "github:nix-community/poetry2nix";

  outputs = { self, nixpkgs, flake-utils, haskellNix, poetry2nix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        overlay = nixpkgs.lib.composeManyExtensions [
          (self: _: {
            hsPkgs = self.haskell-nix.project' rec {
              src = ./.;
              compiler-nix-name = "ghc8107";
              shell = {
                tools = {
                  brittany = { };
                  cabal = { };
                  ghc-prof-flamegraph = { };
                  ghcid = { };
                  haskell-language-server = { };
                  hlint = { };
                  hoogle = { };
                  # stylish-haskell = { };
                  tasty-discover = { };
                };

                # Non-Haskell shell tools.
                buildInputs = with pkgs; [
                  graphviz
                  poetry
                  pyright
                  pythonPackages.pytest
                  # pythonPackages.pytest-watch
                  python39
                ];

                withHoogle = false;
              };
            };
          })
          poetry2nix.overlay
          (final: prev: {
            day24 = prev.poetry2nix.mkPoetryApplication rec {
              projectDir = ./day24;
              pyproject = ./day24/pyproject.toml;
              poetrylock = ./day24/poetry.lock;
            };
          })
        ];
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ haskellNix.overlay overlay ];
        };
        flake = pkgs.hsPkgs.flake { };
      in flake // {
        defaultPackage =
          flake.packages."aoc-2021-haskell:exe:aoc-2021-haskell-exe";
        apps = { day24 = pkgs.day24; };
      });
}
