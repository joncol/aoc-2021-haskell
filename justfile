# Local Variables:
# mode: makefile
# indent-tabs-mode: nil
# End:
# vim: set ft=make :

# Automatically compile when source files change.
ghcid:
  ghcid --command='cabal repl aoc-2021-haskell-exe --repl-options=-Wwarn'

# Convert Graphviz tree to PNG file view the result.
graphviz:
  #!/usr/bin/env bash
  find . -iname "*.dot" | xargs -i% dot -Tpng % -o %.png
