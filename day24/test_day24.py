from day24_opt import *

def test_dummy():
    file = open('expected_z_1000.txt', 'r')
    expected_lines = file.readlines()
    assert(map(lambda nz: nz[1], calculate_zs(1000)) == map(int, expected_lines))
