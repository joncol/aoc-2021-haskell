from collections import deque
import itertools
import sys

def calculate_zs(count):
    all_digits = itertools.repeat(list(range(9, 0, -1)), 14)
    nums = itertools.product(*all_digits)
    return map(calculate_z, itertools.islice(nums, count))

def step(inp, z, add_x, div, add_y):
    w = int(inp.popleft())
    if z % 26 + add_x != w:
        z //= div
        z *= 26
        z += w + add_y
    else:
        z //= div
    return z

def calculate_z(digits):
    num = ''.join(map(str, digits))
    inp = deque(num)
    z = 0

    z = step(inp, z, 12, 1, 6)
    z = step(inp, z, 10, 1, 2)
    z = step(inp, z, 10, 1, 13)
    z = step(inp, z, -6, 26, 8)
    z = step(inp, z, 11, 1, 13)
    z = step(inp, z, -12, 26, 8)
    z = step(inp, z, 11, 1, 3)
    z = step(inp, z, 12, 1, 11)
    z = step(inp, z, 12, 1, 10)
    z = step(inp, z, -2, 26, 8)
    z = step(inp, z, -5, 26, 14)
    z = step(inp, z, -4, 26, 6)
    z = step(inp, z, -4, 26, 8)
    z = step(inp, z, -12, 26, 2)

    return (num, z)

def main():
    count = int(sys.argv[1]) if len(sys.argv) > 1 else None
    # zs = calculate_zs(count)
    print(calculate_z(list("99298993199873")))
    print(calculate_z(list("73181221197111")))
    # print(list(itertools.dropwhile(lambda nz: nz[1] > 400000000, zs)))
    # print(list(itertools.dropwhile(lambda nz: nz[1] != 0, zs))[0])

if __name__ == "__main__":
    main()
