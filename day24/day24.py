from collections import deque
import itertools
import sys

def calculate_zs(count):
    all_digits = itertools.repeat(list(range(9, 0, -1)), 14)
    nums = itertools.product(*all_digits)
    return map(calculate_z, itertools.islice(nums, count))

def calculate_z(digits):
    num = ''.join(map(str, digits))
    inp = deque(num)
    x = y = z = w = 0

    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 12
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 6
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 10
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 2
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 10
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 13
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -6
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 8
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 11
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 13
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -12
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 8
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 11
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 3
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 12
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 11
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 1
    x += 12
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 10
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -2
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 8
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -5
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 14
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -4
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 6
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -4
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 8
    y *= x
    z += y
    w = int(inp.popleft())
    x *= 0
    x += z
    x %= 26
    z //= 26
    x += -12
    x = 1 if x == w else 0
    x = 1 if x == 0 else 0
    y *= 0
    y += 25
    y *= x
    y += 1
    z *= y
    y *= 0
    y += w
    y += 2
    y *= x
    z += y

    return (num, z)

def main():
    count = int(sys.argv[1]) if len(sys.argv) > 1 else None
    zs = calculate_zs(count)
    print(zs)
    # print(list(itertools.dropwhile(lambda nz: nz[1] != 0, zs))[0])

if __name__ == "__main__":
    main()
