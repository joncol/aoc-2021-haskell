module Day22 where

import Control.Monad (void)
import Data.List (foldl')
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char

import qualified Data.Text.IO as TIO

import Parser

type Step = (Power, Cube)
data Power = On | Off deriving (Eq, Ord, Show)
type Cube = (Range, Range, Range)
type Range = (Int, Int)

part1 :: FilePath -> IO Int
part1 filename = do
  steps <- parse (some . lexeme $ parseStep) =<< TIO.readFile filename
  let steps' = filter
        (\(_, ((x1, x2), (y1, y2), (z1, z2))) ->
          maximum (map abs [x1, x2, y1, y2, z1, z2]) <= 50
        )
        steps
  pure . reactorVolume $ steps'

part2 :: FilePath -> IO Int
part2 filename = do
  steps <- parse (some . lexeme $ parseStep) =<< TIO.readFile filename
  pure . reactorVolume $ steps

parseStep :: Parser Step
parseStep = do
  s <- lexeme state
  void $ string "x="
  xr <- range
  void $ string ",y="
  yr <- range
  void $ string ",z="
  zr <- range
  pure (s, (xr, yr, zr))

  where
    state :: Parser Power
    state = (On <$ string "on") <|> (Off <$ string "off")

    range :: Parser Range
    range = do
      l <- signedInteger
      void $ string ".."
      u <- signedInteger
      pure (l, u)
      -- pure (max (-bound) . min bound $ l, max (-bound) . min bound $ u)

reactorVolume :: [Step] -> Int
reactorVolume = fst . foldl' go (0, []) . reverse
  where
    go :: (Int, [Cube]) -> Step -> (Int, [Cube])
    go (vol, cubesProcessed) (pwr, c) = if pwr == On
      then (vol - overlap cubesProcessed c + volume c, c : cubesProcessed)
      else (vol, c : cubesProcessed)

overlap :: [Cube] -> Cube -> Int
overlap cubes (xc, yc, zc) = fst . foldl' go (0, tail cubes) $ cubes
  where
    go :: (Int, [Cube]) -> Cube -> (Int, [Cube])
    go (vol, remCubes) (xc', yc', zc') =
      let ox   = rangeOverlap xc xc'
          oy   = rangeOverlap yc yc'
          oz   = rangeOverlap zc zc'
          lx   = len ox
          ly   = len oy
          lz   = len oz
          vol' = vol + volume (ox, oy, oz) - overlap remCubes (ox, oy, oz)
      in  if lx >= 0 && ly >= 0 && lz >= 0
            then (vol', tail remCubes)
            else (vol, tail remCubes)

len :: Range -> Int
len = uncurry . flip $ (-)

rangeOverlap :: Range -> Range -> Range
rangeOverlap (a1, a2) (b1, b2) = (max a1 b1, min a2 b2)

volume :: Cube -> Int
volume ((x1, x2), (y1, y2), (z1, z2)) =
  (x2 - x1 + 1) * (y2 - y1 + 1) * (z2 - z1 + 1)
