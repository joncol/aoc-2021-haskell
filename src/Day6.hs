module Day6 where

import Data.List (foldl')
import Optics

import qualified Data.Text.IO as TIO

import Parser

type Fish = [Int]

noFish :: Fish
noFish = replicate 9 0

part1 :: FilePath -> IO Int
part1 filename = solver filename 80

part2 :: FilePath -> IO Int
part2 filename = solver filename 256

solver :: FilePath -> Int -> IO Int
solver filename n = do
  fishDays <- parse parseCommaSeparatedNumbers =<< TIO.readFile filename
  let initialFish = foldl' (\acc x -> acc & ix x %~ (+ 1)) noFish fishDays
  pure . sum $ iterate evolveFish initialFish !! n

evolveFish :: Fish -> Fish
evolveFish []         = []
evolveFish (n : fish) = fish ++ [n] & ix 6 %~ (+ n)
