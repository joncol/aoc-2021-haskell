module Day13 where

import Control.Applicative (Alternative((<|>)))
import Control.Applicative.Combinators (some)
import Control.Monad (void, forM_)
import Data.Function (on)
import Data.List (foldl', maximumBy)
import Data.Set (Set)
import GHC.Generics (Generic)
import Text.Megaparsec.Char (char, string)

import qualified Data.Set as Set
import qualified Data.Text.IO as TIO
import qualified Text.Megaparsec.Char.Lexer as L

import Parser

type Point = (Int, Int)

data Line = HorizontalLine Int | VerticalLine Int
  deriving (Eq, Ord, Show, Generic)

part1 :: FilePath -> IO Int
part1 filename = do
  (points, folds) <- parsePointsAndFolds filename
  pure . Set.size $ foldPoints points (head folds)

part2 :: FilePath -> IO Int
part2 filename = do
  (points, folds) <- parsePointsAndFolds filename
  let result = foldl' foldPoints points folds
  printPoints result
  pure 0

parsePointsAndFolds :: FilePath -> IO (Set Point, [Line])
parsePointsAndFolds filename = do
  (points, folds) <- parse parseOrigamiData =<< TIO.readFile filename
  let pointSet = foldl' (\s (x, y) -> Set.insert (x, y) s) Set.empty points
  pure (pointSet, folds)

parseOrigamiData :: Parser ([Point], [Line])
parseOrigamiData = (,) <$> some (lexeme point) <*> some (lexeme line)
  where
    point :: Parser Point
    point = (,) <$> (L.decimal <* char ',') <*> L.decimal

    line :: Parser Line
    line = do
      void $ string "fold along "
      hLine <|> vLine

    hLine :: Parser Line
    hLine = HorizontalLine <$> (string "y=" *> L.decimal)

    vLine :: Parser Line
    vLine = VerticalLine <$> (string "x=" *> L.decimal)

foldPoints :: Set Point -> Line -> Set Point
foldPoints points (HorizontalLine ly) =
  Set.map (\(x, y) -> if y > ly then (x, 2 * ly - y) else (x, y)) points
foldPoints points (VerticalLine lx) =
  Set.map (\(x, y) -> if x > lx then (2 * lx - x, y) else (x, y)) points

printPoints :: Set Point -> IO ()
printPoints points = do
  forM_ [0 .. maxY] $ \y -> do
    forM_ [0 .. maxX] $ \x -> do
      if (x, y) `Set.member` points then putStr "#" else putStr " "
    putStrLn ""
  where
    maxX = fst . maximumBy (compare `on` fst) $ Set.elems points
    maxY = snd . maximumBy (compare `on` snd) $ Set.elems points
