module Day7 where

import Data.Function (on)
import Data.List (minimumBy, sort)

import qualified Data.Text.IO as TIO

import Parser

part1 :: FilePath -> IO Int
part1 filename = do
  crabs <- parse parseCommaSeparatedNumbers =<< TIO.readFile filename
  let m = median crabs
  pure . sum . map (\x -> abs $ x - m) $ crabs

median :: Ord a => [a] -> a
median xs = sort xs !! (length xs `div` 2)

part2 :: FilePath -> IO Int
part2 filename = do
  crabs <- parse parseCommaSeparatedNumbers =<< TIO.readFile filename
  let xs = map (\x -> (x, cost crabs x)) [minimum crabs .. maximum crabs]
  let m  = fst . minimumBy (compare `on` snd) $ xs
  pure . cost crabs $ m

cost :: Integral a => [a] -> a -> a
cost xs m = sum . map (\x -> let d = abs $ x - m in d * (d + 1) `div` 2) $ xs
