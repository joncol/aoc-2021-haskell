module Day5 where

import Control.Applicative.Combinators (some)
import Control.Monad (void)
import Data.List (foldl')
import Data.Map (Map)
import Data.Range ((+=+), fromRanges)
import GHC.Generics (Generic)
import Optics
import Text.Megaparsec.Char (char, string)
import Text.Megaparsec.Char.Lexer (decimal)

import qualified Data.Map as Map
import qualified Data.Text.IO as TIO

import Parser

type Canvas = Map Point Int

emptyCanvas :: Canvas
emptyCanvas = Map.empty

data Point = Point
  { x :: Int
  , y :: Int
  }
  deriving (Eq, Ord, Show, Generic)

parsePoint :: Parser Point
parsePoint = do
  x <- decimal
  void $ char ','
  Point x <$> decimal

data Line = Line
  { p1 :: Point
  , p2 :: Point
  }
  deriving (Eq, Ord, Show, Generic)

isHorizontal :: Line -> Bool
isHorizontal Line {..} = p1 ^. #y == p2 ^. #y

isVertical :: Line -> Bool
isVertical Line {..} = p1 ^. #x == p2 ^. #x

isDiagonal :: Line -> Bool
isDiagonal Line {..} = abs (p1 ^. #x - p2 ^. #x) == abs (p1 ^. #y - p2 ^. #y)

pointsOnLine :: Line -> [Point]
pointsOnLine line@Line {..}
  | isHorizontal line = map (`Point` (p1 ^. #y)) $ fromRanges [x1 +=+ x2]
  | isVertical line = map (Point x1) $ fromRanges [y1 +=+ y2]
  | isDiagonal line = map (\x -> Point x (y1 + (x - x1) * k))
  $ fromRanges [x1 +=+ x2]
  | otherwise = error "Unsupported line slope"
  where
    x1 = p1 ^. #x
    y1 = p1 ^. #y
    x2 = p2 ^. #x
    y2 = p2 ^. #y
    k  = (y2 - y1) `div` (x2 - x1)

drawLine :: Canvas -> Line -> Canvas
drawLine canvas line = foldl' drawPixel canvas (pointsOnLine line)

drawPixel :: Canvas -> Point -> Canvas
drawPixel canvas p = canvas & at p %~ Just . maybe 1 (+ 1)

parseLine :: Parser Line
parseLine = do
  p1 <- lexeme parsePoint
  void . lexeme $ string "->"
  p2 <- lexeme parsePoint
  pure $ Line p1 p2

part1 :: FilePath -> IO Int
part1 filename = do
  contents <- TIO.readFile filename
  allLines <- parse (some parseLine) contents
  let axisLines = filter (\l -> isHorizontal l || isVertical l) allLines
  let canvas    = foldl' drawLine emptyCanvas axisLines
  pure . length . Map.keys . Map.filter (>= 2) $ canvas

part2 :: FilePath -> IO Int
part2 filename = do
  contents <- TIO.readFile filename
  allLines <- parse (some parseLine) contents
  let canvas = foldl' drawLine emptyCanvas allLines
  pure . length . Map.keys . Map.filter (>= 2) $ canvas
