module Day4
  ( part1
  , part2
  ) where

import Control.Applicative.Combinators
import Data.List (elemIndex, find)
import GHC.Generics (Generic)
import Optics
import Optics.Operators.Unsafe ((^?!))

import qualified Data.Set as Set
import qualified Data.Text.IO as TIO
import qualified Text.Megaparsec.Char.Lexer as L

import Parser

data BingoData = BingoData
  { numbers :: [Int]
  , boards  :: [Board]
  }
  deriving stock (Eq, Ord, Show, Generic)

type Pos = Int

data Board = Board
  { numbers :: [Int]
  , marked  :: [Pos] -- ^ Marked position indices.
  }
  deriving stock (Eq, Ord, Show, Generic)

markNumber :: Int -> Board -> Board
markNumber n board = case mPos of
  Just pos -> board & #marked %~ (++ [pos])
  Nothing  -> board
  where mPos = elemIndex n $ board ^. #numbers

isBingo :: Board -> Bool
isBingo Board { marked } = any (all (`elem` marked)) $ allRows <> allCols
  where
    ns      = [0 .. 4]
    allRows = map row ns
    allCols = map col ns
    row n = map (\i -> n * 5 + i) ns
    col n = map (\i -> i * 5 + n) ns

part1 :: FilePath -> IO Int
part1 filename = do
  contents  <- TIO.readFile filename
  bingoData <- parse parseBingoData contents
  case findWinningBoard (bingoData ^. #numbers) (bingoData ^. #boards) of
    Just (board, n) -> do
      let unmarkedPositions =
            Set.toList $ Set.fromList [0 .. 24] `Set.difference` Set.fromList
              (board ^. #marked)

      let sum' = sum (map (\i -> board ^?! #numbers % ix i) unmarkedPositions)

      pure $ sum' * n
    Nothing -> pure 0

findWinningBoard :: [Int] -> [Board] -> Maybe (Board, Int)
findWinningBoard []       _      = Nothing
findWinningBoard (n : ns) boards = case find isBingo boards' of
  Just winningBoard -> Just (winningBoard, n)
  Nothing           -> findWinningBoard ns boards'
  where boards' = map (markNumber n) boards

part2 :: FilePath -> IO Int
part2 filename = do
  contents  <- TIO.readFile filename
  bingoData <- parse parseBingoData contents

  case findLastWinningBoard (bingoData ^. #numbers) (bingoData ^. #boards) of
    Just (board, n) -> do
      let unmarkedPositions =
            Set.toList $ Set.fromList [0 .. 24] `Set.difference` Set.fromList
              (board ^. #marked)

      let sum' = sum (map (\i -> board ^?! #numbers % ix i) unmarkedPositions)

      pure $ sum' * n
    Nothing -> pure 0

findLastWinningBoard :: [Int] -> [Board] -> Maybe (Board, Int)
findLastWinningBoard [] _ = Nothing
findLastWinningBoard (n : ns) [board]
  | isBingo board' = Just (board', n)
  | otherwise      = findLastWinningBoard ns [board']
  where board' = markNumber n board
findLastWinningBoard (n : ns) boards = findLastWinningBoard ns boards'
  where boards' = filter (not . isBingo) (map (markNumber n) boards)

parseBingoData :: Parser BingoData
parseBingoData = do
  numbers <- parseCommaSeparatedNumbers
  boards  <- some parseBoard
  pure $ BingoData numbers boards

parseBoard :: Parser Board
parseBoard = do
  numbers <- lexeme $ count 25 (lexeme L.decimal)
  pure $ Board { marked = [], .. }
