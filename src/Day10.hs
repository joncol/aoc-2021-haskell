module Day10 where

import Control.Monad (foldM)
import Data.Either (lefts, rights)
import Data.List (foldl', sort)
import Text.Megaparsec (someTill, oneOf, eof)
import Text.Megaparsec.Char (eol)

import qualified Data.Text.IO as TIO

import Parser

part1 :: FilePath -> IO Int
part1 filename = do
  sum
    .   map
          (\case
            ')' -> 3
            ']' -> 57
            '}' -> 1197
            '>' -> 25137
            ch  -> error $ "Unepected character in result: " <> show ch
          )
    .   lefts
    .   map balanceBrackets
    <$> (parse (parseLine `someTill` eof) =<< TIO.readFile filename)

part2 :: FilePath -> IO Int
part2 filename = do
  scores <-
    sort
    .   map scoreCompl
    .   rights
    .   map balanceBrackets
    <$> (parse (parseLine `someTill` eof) =<< TIO.readFile filename)
  pure $ scores !! (length scores `div` 2)
  where
    scoreCompl = foldl'
      (\score ch ->
        score
          * 5
          + (case ch of
              '(' -> 1
              '[' -> 2
              '{' -> 3
              '<' -> 4
              _   -> error $ "Unexpected char: " <> show ch
            )
      )
      0

parseLine :: Parser String
parseLine = oneOf ("([{<>}])" :: String) `someTill` eol

balanceBrackets :: String -> Either Char [Char]
balanceBrackets = foldM go []
  where
    go :: [Char] -> Char -> Either Char [Char]
    go xs ch
      | ch `elem` ("([{<" :: String) = Right (ch : xs)
      | ch `elem` (">}])" :: String) = case xs of
        []        -> Left ch
        (x : xs') -> if match x == ch then Right xs' else Left ch
      | otherwise = error $ "Unexpected character: " <> show ch
      where
        match b | b == '('  = ')'
                | b == '['  = ']'
                | b == '{'  = '}'
                | b == '<'  = '>'
                | otherwise = error $ "Not a closing bracket: " <> show b
