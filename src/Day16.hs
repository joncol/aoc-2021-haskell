module Day16 where

import Data.List (foldl')
import Data.Maybe (fromJust)
import Data.Text (Text)
import Data.Text.Internal.Read (hexDigitToInt)
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char (hexDigitChar, binDigitChar)
import Text.Printf (printf)

import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import Lib
import Parser

data Packet = Packet
  { header   :: Header
  , contents :: PacketContents
  }
  deriving (Eq, Ord, Show)

data PacketContents = Literal Int | Operator [Packet] deriving (Eq, Ord, Show)

data Header = Header
  { version    :: Int
  , packetType :: Int
  }
  deriving (Eq, Ord, Show)

part1 :: FilePath -> IO Int
part1 filename = do
  hexDigits <- parse parseHexString =<< TIO.readFile filename
  let binString = hexToBinary hexDigits
  packetVersionSum <$> parse parsePacket binString

part2 :: FilePath -> IO Int
part2 filename = do
  hexDigits <- parse parseHexString =<< TIO.readFile filename
  let binString = hexToBinary hexDigits
  evalPacket <$> parse parsePacket binString

packetVersionSum :: Packet -> Int
packetVersionSum Packet {..} = version header + contentsVersionSum contents

contentsVersionSum :: PacketContents -> Int
contentsVersionSum (Literal  _ ) = 0
contentsVersionSum (Operator ps) = sum . map packetVersionSum $ ps

evalPacket :: Packet -> Int
evalPacket Packet { header = Header {..}, ..}
  | packetType == 0
  = sum . evalContents $ contents
  | packetType == 1
  = product . evalContents $ contents
  | packetType == 2
  = minimum . evalContents $ contents
  | packetType == 3
  = maximum . evalContents $ contents
  | packetType == 4
  = head . evalContents $ contents
  | packetType == 5
  = let lhs = head . evalContents $ contents
        rhs = evalContents contents !! 1
    in  if lhs > rhs then 1 else 0
  | packetType == 6
  = let lhs = head . evalContents $ contents
        rhs = evalContents contents !! 1
    in  if lhs < rhs then 1 else 0
  | packetType == 7
  = let lhs = head . evalContents $ contents
        rhs = evalContents contents !! 1
    in  if lhs == rhs then 1 else 0
  | otherwise
  = error $ "Invalid packet type: " <> show packetType

evalContents :: PacketContents -> [Int]
evalContents (Literal  n ) = [n]
evalContents (Operator ps) = map evalPacket ps

parsePacket :: Parser Packet
parsePacket = do
  header <- parseHeader
  if packetType header == 4
    then do
      n <- parseLiteral
      pure Packet { header, contents = Literal n }
    else do
      opPackets <- parseOperator
      pure Packet { header, contents = Operator opPackets }

parseHeader :: Parser Header
parseHeader = do
  version    <- fromJust . readBin <$> count 3 binDigitChar
  packetType <- fromJust . readBin <$> count 3 binDigitChar
  pure Header { .. }

parseLiteral :: Parser Int
parseLiteral = fromJust . readBin <$> parseGroup
  where
    parseGroup :: Parser String
    parseGroup = do
      lastGroup <- (== '0') <$> binDigitChar
      binDigits <- count 4 binDigitChar
      if not lastGroup then (binDigits ++) <$> parseGroup else pure binDigits

parseOperator :: Parser [Packet]
parseOperator = do
  lengthTypeID <- binDigitChar
  if lengthTypeID == '0'
    then do
      bitCount <- fromJust . readBin <$> count 15 binDigitChar
      s        <- count bitCount binDigitChar
      case runParser (some parsePacket) "" (T.pack s) of
        Right ps  -> pure ps
        Left  err -> error $ "Parse error: " <> show err
    else do
      packetCount <- fromJust . readBin <$> count 11 binDigitChar
      count packetCount parsePacket

parseHexString :: Parser Text
parseHexString = T.pack <$> some hexDigitChar

hexToBinary :: Text -> Text
hexToBinary =
  T.pack . foldl' (\acc ch -> acc ++ hexDigitToBinary ch) "" . T.unpack
  where hexDigitToBinary x = printf "%04b" $ hexDigitToInt x
