{-# LANGUAGE FlexibleContexts #-}

module Day15 where

import Control.Monad (foldM)
import Control.Monad.ST (ST, runST)
import Data.Array
import Data.Array.MArray
import Data.Array.ST
import Data.Char (digitToInt)
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char

import qualified Data.Set as Set
import qualified Data.Text.IO as TIO

import Parser

type Point = (Int, Int)

part1 :: FilePath -> IO Int
part1 filename = do
  (minDists, _prev) <- dijkstra (0, 0) <$> riskLevelsArray filename
  pure . last . elems $ minDists

part2 :: FilePath -> IO Int
part2 filename = do
  riskLevels <- parse parseRiskLevels =<< TIO.readFile filename
  let w1             = length (head riskLevels)
  let h1             = length riskLevels
  let w              = 5 * w1
  let h              = 5 * h1
  let riskLevelArray = listArray ((0, 0), (w1 - 1, h1 - 1)) (concat riskLevels)

  let riskLevelArray' = array
        ((0, 0), (w - 1, h - 1))
        [ ((y, x), getRisk w1 h1 riskLevelArray x y)
        | y <- [0 .. h - 1]
        , x <- [0 .. w - 1]
        ]

  let (minDists, _prev) = dijkstra (0, 0) riskLevelArray'
  pure $ minDists ! (h - 1, w - 1)
  where
    getRisk :: Int -> Int -> Array Point Int -> Int -> Int -> Int
    getRisk w1 h1 a x y =
      let diff = x `div` w1 + y `div` w1
          risk = (a ! (y `mod` h1, x `mod` w1)) + diff
      in  1 + ((risk - 1) `mod` 9)

riskLevelsArray :: FilePath -> IO (Array Point Int)
riskLevelsArray filename = do
  riskLevels <- parse parseRiskLevels =<< TIO.readFile filename
  let w = length $ head riskLevels
  let h = length riskLevels
  pure $ listArray ((0, 0), (w - 1, h - 1)) (concat riskLevels)

parseRiskLevels :: Parser [[Int]]
parseRiskLevels = some $ map digitToInt <$> digitChar `someTill` eol

dijkstra :: Point -> Array Point Int -> (Array Point Int, Array Point Point)
dijkstra src graph = runST $ do
  minDists <- newSTArray b (maxBound :: Int)
  writeArray minDists src 0
  prev <- newSTArray b invalidIndex

  let
    go q = case Set.minView q of
      Nothing -> pure ()
      Just ((dist, u), q') ->
        let
          relax qAcc (weight, v) = do
            let distViaU = dist + weight
            oldDist <- readArray minDists v
            if distViaU >= oldDist
              then pure qAcc
              else do
                writeArray minDists v distViaU
                writeArray prev     v u
                pure . Set.insert (distViaU, v) . Set.delete (oldDist, v) $ qAcc
        in  foldM relax q' (getNeighbors u) >>= go
  go (Set.singleton (0, src))
  m <- freeze minDists
  p <- freeze prev
  pure (m, p)

  where
    newSTArray :: Ix i => (i, i) -> e -> ST s (STArray s i e)
    newSTArray             = newArray

    invalidIndex           = (-1, -1)
    b@((y1, x1), (y2, x2)) = bounds graph

    getNeighbors :: Point -> [(Int, Point)]
    getNeighbors (vy, vx) = map (\pn -> (graph ! pn, pn)) ns
      where
        ns = filter withinBounds
                    [(vy, vx + 1), (vy, vx - 1), (vy + 1, vx), (vy - 1, vx)]

    withinBounds :: Point -> Bool
    withinBounds (y, x) | x < x1 || x > x2 = False
                        | y < y1 || y > y2 = False
                        | otherwise        = True
