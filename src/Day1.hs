module Day1 where

import Control.Applicative (ZipList(..))
import Data.List (tails)

import Lib (readInts)

part1 :: FilePath -> IO Int
part1 filename = do
  xs <- readInts filename
  let diffs = zipWith (-) xs (tail xs)
  pure . length . filter (< 0) $ diffs

part2 :: FilePath -> IO Int
part2 filename = do
  sums <- map sum . windows 3 <$> readInts filename
  let diffs = zipWith (-) sums (tail sums)
  pure . length . filter (< 0) $ diffs

windows :: Int -> [a] -> [[a]]
windows n = getZipList . traverse ZipList . take n . tails
