module Day3
  (
    part1
  , part2
  ) where

import Data.Bits ((.&.), complement, shiftR)
import Data.Digits (unDigits)
import Data.Either (rights, fromRight)
import Data.List (find)
import Data.Maybe (fromJust)
import Text.Megaparsec (runParser)

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Text.Megaparsec.Char.Lexer as L

import Lib
import Parser (Parser)

part1 :: FilePath -> IO Int
part1 filename = do
  ls <- readStrings filename
  let transposed  = transpose ls
  let gammaDigits = map mostCommon transposed
  let gammaRate   = unDigits 2 gammaDigits
  let epsilonRate = complement gammaRate .&. 0x0fff
  pure $ gammaRate * epsilonRate
  where
    mostCommon :: String -> Int
    mostCommon xs =
      if (length . filter (== '0') $ xs) > length xs `div` 2 then 0 else 1

part2 :: FilePath -> IO Int
part2 filename = do
  ls      <- readTexts filename
  revNums <- parseRevBinaryNumbers filename

  let oxyIndex = findOnlyRemaining findOxy revNums
  let co2Index = findOnlyRemaining findCO2 revNums
  let oxyRating =
        fromRight 0 (runParser (L.binary :: Parser Int) "" $ ls !! oxyIndex)
  let co2Rating =
        fromRight 0 (runParser (L.binary :: Parser Int) "" $ ls !! co2Index)
  pure $ oxyRating * co2Rating
  where
    findOnlyRemaining f nums =
      fst . head . fromJust . find ((== 1) . length) $ iterate
        f
        (zip [0 ..] nums)

    findOxy :: [(Int, Int)] -> [(Int, Int)]
    findOxy xs =
      map (\(i, x) -> (i, x `shiftR` 1))
      . filter (\(_, x) -> x .&. 1 == keep)
      $ xs
      where keep = mostCommonFirstBit . map snd $ xs

    findCO2 :: [(Int, Int)] -> [(Int, Int)]
    findCO2 xs =
      map (\(i, x) -> (i, x `shiftR` 1))
      . filter (\(_, x) -> x .&. 1 == keep)
      $ xs
      where keep = 1 - (mostCommonFirstBit . map snd $ xs)

    mostCommonFirstBit
      :: [Int] -- ^ List of numbers to check.
      -> Int -- ^ 0 or 1, the most common bit at least significant position.
    mostCommonFirstBit xs =
      if (length . filter (== 0) $ map (.&. 1) xs) > length xs `div` 2
        then 0
        else 1

parseRevBinaryNumbers :: FilePath -> IO [Int]
parseRevBinaryNumbers filename = do
  revLines <-
    map T.reverse . filter (not . T.null) . T.lines <$> TIO.readFile filename
  pure $ rights (map (runParser (L.binary :: Parser Int) "") revLines)
