module Day18 where

import Control.Applicative (Alternative((<|>)))
import Control.Monad (void)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.Maybe (fromJust, fromMaybe)
import Math.Combinat (BinTree(..))
import Text.Megaparsec (some)
import Text.Megaparsec.Char (char)

import qualified Data.Text.IO as TIO
import qualified Text.Megaparsec.Char.Lexer as L

import Parser

data Action = Explode Int Int | Split Int Int deriving (Eq, Show)

type Tree a = BinTree a

height :: Tree a -> Int
height (Leaf _    ) = 0
height (Branch l r) = 1 + max (height l) (height r)

data Crumb a = LeftCrumb (Tree a) | RightCrumb (Tree a) deriving (Eq, Show)
type Breadcrumbs a = [Crumb a]

type TreeZipper a = (Tree a, Breadcrumbs a)

zipper :: Tree a -> TreeZipper a
zipper t = (t, [])

goLeft :: TreeZipper a -> Maybe (TreeZipper a)
goLeft (Branch l r, bs) = Just (l, LeftCrumb r : bs)
goLeft _                = Nothing

goRight :: TreeZipper a -> Maybe (TreeZipper a)
goRight (Branch l r, bs) = Just (r, RightCrumb l : bs)
goRight _                = Nothing

goUp :: TreeZipper a -> Maybe (TreeZipper a)
goUp (t, LeftCrumb r : bs ) = Just (Branch t r, bs)
goUp (t, RightCrumb l : bs) = Just (Branch l t, bs)
goUp (_, []               ) = Nothing

goTop :: TreeZipper a -> TreeZipper a
goTop z@(_, []) = z
goTop z         = goTop . fromJust . goUp $ z

goLeftMost :: TreeZipper a -> TreeZipper a
goLeftMost z = maybe z goLeftMost (goLeft z)

goRightMost :: TreeZipper a -> TreeZipper a
goRightMost z = maybe z goRightMost (goRight z)

goNextLeaf :: TreeZipper a -> Maybe (TreeZipper a)
goNextLeaf (  _, []              ) = Nothing
goNextLeaf z@(_, RightCrumb _ : _) = pure z >>= goUp >>= goNextLeaf
goNextLeaf z@(_, LeftCrumb _ : _) =
  (pure z >>= goUp >>= goRight) <&> goLeftMost

goPrevLeaf :: TreeZipper a -> Maybe (TreeZipper a)
goPrevLeaf (  _, []             ) = Nothing
goPrevLeaf z@(_, LeftCrumb _ : _) = pure z >>= goUp >>= goPrevLeaf
goPrevLeaf z@(_, RightCrumb _ : _) =
  (pure z >>= goUp >>= goLeft) <&> goRightMost

value :: TreeZipper a -> Maybe a
value (Leaf x    , _) = Just x
value (Branch _ _, _) = Nothing

depth :: TreeZipper a -> Int
depth (_, bs) = length bs

modify :: (a -> a) -> TreeZipper a -> TreeZipper a
modify f (Leaf x    , bs) = (Leaf $ f x, bs)
modify _ (Branch l r, bs) = (Branch l r, bs)

attach :: Tree a -> TreeZipper a -> TreeZipper a
attach t (_, bs) = (t, bs)

part1 :: FilePath -> IO Int
part1 filename = do
  trees <- parse (some $ lexeme parseTree) =<< TIO.readFile filename
  let res = foldl1 (\acc t -> findStable process $ Branch acc t) trees
  pure $ magnitude res

part2 :: FilePath -> IO Int
part2 filename = do
  trees <- parse (some $ lexeme parseTree) =<< TIO.readFile filename
  pure
    . maximum
    . map magnitude
    $ [ findStable process $ Branch x y | x <- trees, y <- trees, x /= y ]

parseTree :: Parser (Tree Int)
parseTree = do
  void $ char '['
  l <- Leaf <$> L.decimal <|> parseTree
  void $ char ','
  r <- Leaf <$> L.decimal <|> parseTree
  void $ char ']'
  pure $ Branch l r

findStable :: (Tree Int -> Tree Int) -> Tree Int -> Tree Int
findStable f t = let t' = f t in if t' == t then t else findStable f t'

-- | Do a single action.
process :: Tree Int -> Tree Int
process tree = if height tree >= 5
  then
    let (z, mExplAction) = findNextExplodeAction (zipper tree & goLeftMost)
    in  fst $ doAction (fromJust mExplAction) z
  else
    let (z, mSplitAction) = findNextSplitAction (zipper tree & goLeftMost)
    in  case mSplitAction of
          Just splitAction -> fst $ doAction splitAction z
          Nothing          -> tree

findNextExplodeAction :: TreeZipper Int -> (TreeZipper Int, Maybe Action)
findNextExplodeAction z
  | depth z == 5
  = let x1 = val
        x2 = fromJust $ pure z >>= goUp >>= goRight >>= value
    in  (z, Just $ Explode x1 x2)
  | otherwise
  = maybe (z, Nothing) findNextExplodeAction (goNextLeaf z)
  where val = fromJust (value z)

findNextSplitAction :: TreeZipper Int -> (TreeZipper Int, Maybe Action)
findNextSplitAction z
  | maybe False (>= 10) (value z)
  = ( z
    , Just $ Split (val `div` 2) (ceiling (fromIntegral val / (2.0 :: Float)))
    )
  | otherwise
  = maybe (z, Nothing) findNextSplitAction (goNextLeaf z)
  where val = fromJust (value z)

doAction :: Action -> TreeZipper Int -> TreeZipper Int
doAction action z = case action of
  Explode x1 x2 ->
    let z' = fromJust $ pure z >>= goUp <&> attach (Leaf 0)
        z'' =
          fromMaybe z' $ pure z' >>= goPrevLeaf >>= goNextLeaf . modify (+ x1)
    in  fromMaybe (z'' & goTop)
          $   pure z''
          >>= goNextLeaf
          <&> modify (+ x2)
          <&> goTop

  Split x1 x2 -> z & attach (Branch (Leaf x1) (Leaf x2)) & goTop

magnitude :: Tree Int -> Int
magnitude (Branch l r) = 3 * magnitude l + 2 * magnitude r
magnitude (Leaf x    ) = x
