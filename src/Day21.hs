module Day21 where

import Control.Monad.State
import Data.List (foldl')
import Data.Map ((!?), Map)
import Data.Tuple (swap)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (decimal)

import qualified Data.Map as Map
import qualified Data.Text.IO as TIO

import Parser

type Game = ((Int, Int), (Int, Int))
type Cache = Map Game (Int, Int)

part1 :: FilePath -> IO Int
part1 filename = do
  (p1, p2) <- parse parseStartingPositions =<< TIO.readFile filename
  let (((_, s1), (_, s2)), n) = runState (play ((p1, 0), (p2, 0))) 0
  pure $ n * min s1 s2

parseStartingPositions :: Parser (Int, Int)
parseStartingPositions = do
  void . lexeme $ string "Player 1 starting position:"
  p1 <- lexeme decimal
  void . lexeme $ string "Player 2 starting position:"
  p2 <- lexeme decimal
  pure (p1, p2)

play :: Game -> State Int Game
play game@((_, s1), (_, s2)) | s1 >= 1000 || s2 >= 1000 = pure game
                             | otherwise                = step game >>= play

step :: Game -> State Int Game
step ((p1, s1), (p2, s2)) = do
  d <- get
  let dieSum = sum [d + 1, d + 2, d + 3]
  modify (+ 3)
  let p1' = move dieSum p1
  pure ((p2, s2), (p1', s1 + p1'))

part2 :: FilePath -> IO Int
part2 filename = do
  (p1, p2) <- parse parseStartingPositions =<< TIO.readFile filename
  let (w1, w2) = evalState (solveDP ((p1, 0), (p2, 0))) Map.empty
  pure $ max w1 w2

solveDP :: Game -> State Cache (Int, Int)
solveDP game@((p1, s1), (p2, s2))
  | s1 >= 21 = pure (1, 0)
  | s2 >= 21 = pure (0, 1)
  | otherwise = do
    cache <- get
    case cache !? game of
      Just res -> pure res
      Nothing  -> do
        res <- swap . foldl' addScores (0, 0) <$> mapM
          solveDP
          [ ((p2, s2), (p1', s1 + p1'))
          | d <- sum <$> replicateM 3 [1, 2, 3]
          , let p1' = move p1 d
          ]
        modify (Map.insert game res)
        pure res
  where addScores (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

move :: Integral a => a -> a -> a
move n p = ((p + n - 1) `mod` 10) + 1
