{-# OPTIONS_GHC -Wno-unused-local-binds #-}
module Day9 where

import Control.Applicative (ZipList(..))
import Control.Monad (forM)
import Control.Monad.Identity (Identity(runIdentity))
import Control.Monad.State (State, evalState, get, modify)
import Data.Array (Array, (//), (!), bounds, elems, indices, listArray)
import Data.Char (digitToInt)
import Data.List ((\\), foldl', group, sort)
import Text.Megaparsec (eof, manyTill)
import Text.Megaparsec.Char (digitChar, eol)

import qualified Data.Text.IO as TIO

import Parser

part1 :: FilePath -> IO Int
part1 filename =
  sum
    .   map (sum . map (\(h, d) -> if d < 0 then h + 1 else 0))
    <$> getDeltaMap filename

part2 :: FilePath -> IO Int
part2 filename = do
  deltaMap <- getDeltaMap filename
  let lowPoints = getLowPoints deltaMap
  let w         = length . head $ lowPoints
  let h         = length lowPoints
  let ary = listArray ((0, 0), (w - 1, h - 1)) (concat lowPoints)
  let filled =
        foldl'
            (\a p ->
              let color = a ! p
              in  if color > 0 then floodFill color p (a // [(p, 0)]) else a
            )
            ary
          $ indices ary
  pure
    $ product
    . take 3
    . reverse
    . sort
    . map length
    . group
    . sort
    . filter (> 0)
    . elems
    $ filled

getDeltaMap :: FilePath -> IO [[(Int, Int)]]
getDeltaMap filename = do
  contents  <- TIO.readFile filename
  heightmap <- parse parseHeightmap contents
  let deltaMap = deltas heightmap
  pure . getZipList $ zip <$> ZipList heightmap <*> ZipList deltaMap

parseHeightmap :: Parser [[Int]]
parseHeightmap = parseLine `manyTill` eof

parseLine :: Parser [Int]
parseLine = map digitToInt <$> digitChar `manyTill` eol

deltas :: [[Int]] -> [[Int]]
deltas m = runIdentity $ do
  forM [0 .. h - 1] $ \y -> do
    forM [0 .. w - 1] $ \x -> do
      let ns   = neighbors w h x y
      let min' = minimum (map (\(i, j) -> m !! j !! i) ns)
      pure $ (m !! y !! x) - min'
  where
    w = length (head m)
    h = length m

neighbors :: Int -> Int -> Int -> Int -> [(Int, Int)]
neighbors w h x y = [ (i, y) | i <- xs ] ++ [ (x, j) | j <- ys ]
  where
    xs = [max (x - 1) 0 .. min (x + 1) (w - 1)] \\ [x]
    ys = [max (y - 1) 0 .. min (y + 1) (h - 1)] \\ [y]

getLowPoints :: [[(Int, Int)]] -> [[Int]]
getLowPoints deltaMap = evalState go 0
  where
    go :: State Int [[Int]]
    go = forM deltaMap $ \row -> do
      forM row $ \(h, d) -> do
        if
          | d < 0     -> modify (+ 1) >> get >>= pure
          | h == 9    -> pure (-1)
          | otherwise -> pure 0

type Point = (Int, Int)

withinBounds :: Array Point Int -> Point -> Bool
withinBounds a (y, x) | x < x1 || x > x2 = False
                      | y < y1 || y > y2 = False
                      | otherwise        = True
  where ((y1, x1), (y2, x2)) = bounds a

floodFill :: Int -> Point -> Array Point Int -> Array Point Int
floodFill color p@(y, x) a | withinBounds a p && a ! p == 0 = a'
                           | otherwise = a
  where
    a' =
      floodFill color (y, x + 1)
        . floodFill color (y    , x - 1)
        . floodFill color (y + 1, x)
        $ floodFill color (y - 1, x) (a // [(p, color)])
