module Day19 where

import Control.Monad (void)
import Data.Bifunctor (second)
import Data.List (group, sort, nub)
import Data.Maybe (listToMaybe)
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char
import Text.Printf (printf)

import qualified Data.Text.IO as TIO

import Lib
import Parser

data Point = Point Int Int Int
  deriving (Eq, Ord)

instance Show Point where
  show (Point x y z) = printf "(%d, %d, %d)\n" x y z

part1 :: FilePath -> IO Int
part1 filename = do
  scanners <- parse (some $ lexeme parseScanner) =<< TIO.readFile filename
  let (beacons, _) = foldScanners (head scanners) (tail scanners) []
  pure . length . nub . sort $ beacons

part2 :: FilePath -> IO Int
part2 filename = do
  scanners <- parse (some $ lexeme parseScanner) =<< TIO.readFile filename
  let (_, scannerPositions) = foldScanners (head scanners) (tail scanners) []
  pure
    . maximum
    $ [ manhattanDist p1 p2 | p1 <- scannerPositions, p2 <- scannerPositions ]

parseScanner :: Parser [Point]
parseScanner = do
  void $ string "--- scanner"
  void $ printChar `someTill` eol
  some parsePoint

parsePoint :: Parser Point
parsePoint = do
  x <- (signedInteger :: Parser Int)
  void $ char ','
  y <- signedInteger
  void $ char ','
  z <- signedInteger
  void newline
  pure $ Point x y z

foldScanners :: [Point] -> [[Point]] -> [Point] -> ([Point], [Point])
foldScanners s0 []       scannerPositions = (s0, scannerPositions)
foldScanners s0 (s : ss) scannerPositions = case findMatch s0 s of
  Just (rn, sp) ->
    let s' = s0 ++ map ((`minus` sp) . (rotFuns !! rn)) s
    in  foldScanners s' ss (sp : scannerPositions)
  Nothing -> foldScanners s0 (ss ++ [s]) scannerPositions

findStable :: Eq a => (a -> a) -> a -> a
findStable f t = let t' = f t in if t' == t then t else findStable f t'

findMatch :: [Point] -> [Point] -> Maybe (Int, Point)
findMatch s0 s =
  listToMaybe . map (second $ head . head) . filter (not . null . snd) $ zip
    [0 ..]
    diffs
  where
    allDiffs = [ [ ry `minus` x | ry <- rotations y ] | x <- s0, y <- s ]
    diffs = map (filter ((>= 12) . length) . group . sort) $ transpose allDiffs

rotations :: Point -> [Point]
rotations p = map ($ p) rotFuns

rotFuns :: [Point -> Point]
rotFuns =
  [ id
  , \(Point x y z) -> Point x (-z) y
  , \(Point x y z) -> Point x (-y) (-z)
  , \(Point x y z) -> Point x z (-y)
  , \(Point x y z) -> Point (-x) (-y) z
  , \(Point x y z) -> Point (-x) (-z) (-y)
  , \(Point x y z) -> Point (-x) y (-z)
  , \(Point x y z) -> Point (-x) z y
  , \(Point x y z) -> Point (-z) x (-y)
  , \(Point x y z) -> Point y x (-z)
  , \(Point x y z) -> Point z x y
  , \(Point x y z) -> Point (-y) x z
  , \(Point x y z) -> Point z (-x) (-y)
  , \(Point x y z) -> Point y (-x) z
  , \(Point x y z) -> Point (-z) (-x) y
  , \(Point x y z) -> Point (-y) (-x) (-z)
  , \(Point x y z) -> Point (-y) (-z) x
  , \(Point x y z) -> Point z (-y) x
  , \(Point x y z) -> Point y z x
  , \(Point x y z) -> Point (-z) y x
  , \(Point x y z) -> Point z y (-x)
  , \(Point x y z) -> Point (-y) z (-x)
  , \(Point x y z) -> Point (-z) (-y) (-x)
  , \(Point x y z) -> Point y (-z) (-x)
  ]

minus :: Point -> Point -> Point
minus (Point lx ly lz) (Point rx ry rz) = Point (lx - rx) (ly - ry) (lz - rz)

plus :: Point -> Point -> Point
plus (Point lx ly lz) (Point rx ry rz) = Point (lx + rx) (ly + ry) (lz + rz)

manhattanDist :: Point -> Point -> Int
manhattanDist (Point x1 y1 z1) (Point x2 y2 z2) =
  abs (x1 - x2) + abs (y1 - y2) + abs (z1 - z2)
