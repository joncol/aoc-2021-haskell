{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}

module Day8 where

import Control.Applicative.Combinators (count, some)
import Control.Monad (void)
import Data.List ((\\), foldl', sort)
import Data.Maybe (fromJust)
import Data.Text (Text)
import Text.Megaparsec (satisfy)
import Text.Megaparsec.Char (char)

import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import Parser

part1 :: FilePath -> IO Int
part1 filename =
  sum
    .   map knownDigitsCount
    <$> (parse (some parseLine) =<< TIO.readFile filename)
  where
    knownDigitsCount (_, val) =
      length . filter (\d -> T.length d `elem` [2, 4, 3, 7]) $ val

part2 :: FilePath -> IO Int
part2 filename = do
  puzzleData <- parse (some parseLine) =<< TIO.readFile filename
  pure . sum . map solveLine $ puzzleData
  where
    solveLine :: ([Text], [Text]) -> Int
    solveLine (patterns, output) =
      fromDigits . flip foldMap output $ \outputDigit ->
        [fromJust . lookup (sort . T.unpack $ outputDigit) $ digits]
      where digits = solvePatterns patterns

    fromDigits :: [Int] -> Int
    fromDigits = foldl' addDigit 0

    addDigit num d = 10 * num + d

parseLine :: Parser ([Text], [Text])
parseLine = do
  signalPatterns <- count 10 word
  void $ lexeme $ char '|'
  outputValue <- count 4 word
  pure (signalPatterns, outputValue)
  where
    word :: Parser Text
    word = T.pack <$> lexeme (some . satisfy $ (`elem` ("abcdefg" :: String)))

-- | Figure out pattern to digit mappings for a single line in the input data.
solvePatterns :: [Text] -> [(String, Int)]
solvePatterns patterns = do
  -- Figure out segments.
  let c = [ ch | (ch, n) <- freqs $ has 6, ch `elem` one, n == 2 ]
  let f = one \\ c

  -- The '3' is the pattern with 5 segments, that also contains the two segments
  -- for the '1'.
  let [three] =
        filter (\s -> Set.fromList one `Set.isSubsetOf` Set.fromList s) $ has 5

  -- The '2' is the pattern with 5 segments, that is not the '3', and that also
  -- contains the 'c' segment.
  let [two] = filter (head c `elem`) $ has 5 \\ [three]

  -- The '5' is the pattern with 5 segments, that is not the '3', that also
  -- contains the 'f' segment.
  let [five] = filter (head f `elem`) $ has 5 \\ [three]

  let e      = two \\ three

  -- The '9' is the pattern with 6 segments, that doesn't contain the 'e'
  -- segment.
  let [nine] = filter (head e `notElem`) $ has 6

  -- The '0' is the pattern with 6 segments, that also contains the 'c' and 'e'
  -- segments.
  let [zero] = filter (\s -> head c `elem` s && head e `elem` s) $ has 6

  -- The '6' is the pattern with 6 segments, that is not the '0' or the '9'.
  let [six] = filter (`notElem` [zero, nine]) $ has 6
  zip [zero, one, two, three, four, five, six, seven, eight, nine] [0 .. 9]

  where
    freqs :: [String] -> [(Char, Int)]
    freqs xs =
      [ (x, n)
      | x <- ['a' .. 'g']
      , let n = (length . filter (== x)) (concat xs)
      , n > 0
      ]

    one   = sort . head . map T.unpack . filter ((== 2) . T.length) $ patterns
    four  = sort . head . map T.unpack . filter ((== 4) . T.length) $ patterns
    seven = sort . head . map T.unpack . filter ((== 3) . T.length) $ patterns
    eight = sort . head . map T.unpack . filter ((== 7) . T.length) $ patterns

    has n = map (sort . T.unpack) . filter ((== n) . T.length) $ patterns
