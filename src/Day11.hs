module Day11 where

import Data.Array ((//), (!), Array, assocs, bounds, listArray)
import Data.Char (digitToInt)
import Data.List ((\\), foldl', union, findIndex)
import Data.Maybe (fromJust)
import Text.Megaparsec (eof, manyTill)
import Text.Megaparsec.Char (digitChar, eol)

import qualified Data.Map as Map
import qualified Data.Text.IO as TIO

import Parser

type Point = (Int, Int)

part1 :: FilePath -> IO Int
part1 filename = sum . map snd . take 101 <$> evolutions filename

part2 :: FilePath -> IO Int
part2 filename = fromJust . findIndex ((== 100) . snd) <$> evolutions filename

evolutions :: FilePath -> IO [(Array Point Int, Int)]
evolutions filename = do
  energyLevels <- parse (parseLine `manyTill` eof) =<< TIO.readFile filename
  let w = length (head energyLevels)
  let h = length energyLevels
  let a = listArray ((0, 0), (w - 1, h - 1)) (concat energyLevels)
  pure $ iterate evolve (a, 0)

parseLine :: Parser [Int]
parseLine = map digitToInt <$> digitChar `manyTill` eol

evolve :: (Array Point Int, Int) -> (Array Point Int, Int)
evolve (a, _) = (aFinal, flashCount)
  where
    a'         = (+ 1) <$> a
    toFlash    = Map.keys . Map.filter (> 9) . Map.fromList . assocs $ a'
    aFlashed   = flash toFlash [] a'
    aFinal     = fmap (\l -> if l > 9 then 0 else l) aFlashed
    flashCount = Map.size . Map.filter (> 9) . Map.fromList . assocs $ aFlashed

flash
  :: [Point] -- ^ The queue of points to flash.
  -> [Point] -- ^ A list of points already flashed this round.
  -> Array Point Int -- ^ Array of current energy levels.
  -> Array Point Int -- ^ Array of energy levels after flash.
flash [] _ energyLevels = energyLevels
flash (p@(y, x) : ps) visited energyLevels
  | p `notElem` visited = flash (ps `union` toFlash) (p : visited) aFlashed
  | otherwise           = flash ps (p : visited) energyLevels
  where
    ((y1, x1), (y2, x2)) = bounds energyLevels

    neighbors :: [Point]
    neighbors =
      [ (ny, nx)
      | ny <- [y - 1 .. y + 1]
      , ny >= y1 && ny <= y2
      , nx <- [x - 1 .. x + 1]
      , nx >= x1 && nx <= x2
      ]
      \\ [(y, x)]

    aFlashed = foldl' (\a pn -> a // [(pn, a ! pn + 1)]) energyLevels neighbors
    toFlash  = filter (\pn -> aFlashed ! pn > 9) neighbors
