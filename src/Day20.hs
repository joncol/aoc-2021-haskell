module Day20 where

import Data.Bits
import Data.Functor (($>))
import Data.List (foldl', unfoldr)
import Data.Set (Set)
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char

import qualified Data.Set as Set
import qualified Data.Text.IO as TIO

import Parser

type ImageAlgo = [Int]
type Image = Set (Int, Int)
type Bounds = ((Int, Int), (Int, Int))

part1 :: FilePath -> IO Int
part1 = solver 2

part2 :: FilePath -> IO Int
part2 = solver 50

solver :: Int -> FilePath -> IO Int
solver stepCount filename = do
  (algo, image) <- parse parseImageData =<< TIO.readFile filename
  let image' = unfoldr (enhance algo) (image, False) !! (stepCount - 1)
  pure $ Set.size image'

parseImageData :: Parser (ImageAlgo, Image)
parseImageData = do
  algo   <- lexeme $ pixel `someTill` eol
  pixels <- some (pixel `someTill` eol)

  let pixels' =
        concat
          . zipWith
              (\y row -> zipWith (\x p -> (x, y, p)) [(0 :: Int) ..] row)
              [(0 :: Int) ..]
          $ pixels

  let image = foldl'
        (\img (x, y, p) -> if p > 0 then Set.insert (x, y) img else img)
        Set.empty
        pixels'

  pure (algo, image)

  where
    pixel :: Parser Int
    pixel = pixelOn <|> pixelOff

    pixelOn, pixelOff :: Parser Int
    pixelOn  = char '#' $> 1
    pixelOff = char '.' $> 0

enhance :: ImageAlgo -> (Image, Bool) -> Maybe (Image, (Image, Bool))
enhance algo (image, flag) = Just (image', (image', not flag))
  where
    bs@((x1, y1), (x2, y2)) = bounds image
    coords                  = [ (x, y) | y <- [y1 .. y2], x <- [x1 .. x2] ]
    image'                  = foldl'
      (\acc (x, y) ->
        if (algo !! getPixelNum bs (if flag then 1 else 0) image (x, y)) == 1
          then Set.insert (x, y) acc
          else Set.delete (x, y) acc
      )
      Set.empty
      coords

getPixelNum :: Bounds -> Int -> Set (Int, Int) -> (Int, Int) -> Int
getPixelNum ((x1, y1), (x2, y2)) boundaryValue image (px, py) = foldl'
  (\acc p -> (acc `shift` 1) .|. if
    | p `Set.member` image -> 1
    | onBoundary p         -> boundaryValue
    | otherwise            -> 0
  )
  0
  mask
  where
    mask = [ (x, y) | y <- [py - 1 .. py + 1], x <- [px - 1 .. px + 1] ]
    onBoundary (x, y) = x <= x1 || x >= x2 || y <= y1 || y >= y2

bounds :: Set (Int, Int) -> Bounds
bounds image = ((x1, y1), (x2, y2))
  where
    ps = Set.toList image
    x1 = (minimum . map fst $ ps) - 1
    y1 = (minimum . map snd $ ps) - 1
    x2 = (maximum . map fst $ ps) + 1
    y2 = (maximum . map snd $ ps) + 1
