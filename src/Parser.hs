module Parser where

import Data.Text (Text)
import Data.Void (Void)
import Text.Megaparsec (Parsec, ParsecT, errorBundlePretty, runParser, runParserT, sepBy)
import Text.Megaparsec.Char (char, space)

import qualified Text.Megaparsec.Char.Lexer as L
import Control.Monad.State (State, runState)

type Parser = Parsec Void Text

type StatefulParser s = ParsecT Void Text (State s)

parse :: Parser a -> Text -> IO a
parse parser str = case runParser parser "" str of
  Right d   -> pure d
  Left  err -> do
    putStrLn . errorBundlePretty $ err
    error "Parser error"

parseStateful :: StatefulParser s a -> Text -> s -> IO (a, s)
parseStateful parser str s = case a of
  Right d   -> pure (d, s')
  Left  err -> do
    putStrLn . errorBundlePretty $ err
    error "Parser error"
  where (a, s') = runState (runParserT parser "" str) s

-- | Parse using the given parser, and then skip any trailing whitespace.
lexeme :: Parser a -> Parser a
lexeme = L.lexeme space

signedInteger :: Num a => Parser a
signedInteger = L.signed space L.decimal

parseCommaSeparatedNumbers :: Parser [Int]
parseCommaSeparatedNumbers = lexeme $ L.decimal `sepBy` char ','
