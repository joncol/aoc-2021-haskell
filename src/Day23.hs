{-# LANGUAGE DeriveAnyClass #-}

module Day23 where

import Control.Monad (foldM, void)
import Control.Monad.ST (runST)
import Data.Bits
import Data.Function ((&), on)
import Data.Functor (($>))
import Data.Hashable
import Data.List ((\\), find, groupBy, sort, sortBy, unfoldr)
import Data.Map ((!?), Map)
import Data.Maybe (catMaybes, fromJust, fromMaybe, mapMaybe)
import Data.STRef.Strict (newSTRef, writeSTRef, readSTRef)
import GHC.Generics (Generic)
import Safe (headDef, lastDef)
import Text.Megaparsec hiding (Pos, parse)
import Text.Megaparsec.Char (char)

import qualified Data.HashTable.Class as H
import qualified Data.HashTable.ST.Basic as HST
import qualified Data.List.Ordered as DLO
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Text.Megaparsec.Char.Lexer as L

import Lib
import Parser hiding (lexeme)

data Amphipod = A | B | C | D deriving (Eq, Ord, Show, Enum, Generic, Hashable)

targetRoomIndex :: Amphipod -> Int
targetRoomIndex = fromEnum

energy :: Amphipod -> Int
energy = (10 ^) . fromEnum

data Pos = Hallway Int | Room Int deriving (Eq, Ord, Show, Generic, Hashable)

isInRoom :: Pos -> Bool
isInRoom (Hallway _) = False
isInRoom (Room    _) = True

-- | Get all possible hallway positions.
hallway :: [Pos]
hallway = map Hallway [0 .. 10] \\ invalidHallwayPositions

invalidHallwayPositions :: [Pos]
invalidHallwayPositions = map Hallway [2, 4, 6, 8]

-- | Get all possible room positions.
rooms :: Int -> [Pos]
rooms roomSize = map Room [0 .. 4 * roomSize - 1]

roomIndex :: Pos -> Int
roomIndex (Room n) = n `mod` 4
roomIndex x        = error $ "not a room position: " <> show x

nearestHallwayCoord :: Pos -> Int
nearestHallwayCoord (Hallway n) = n
nearestHallwayCoord r           = 2 + roomIndex r * 2

toHallway :: Pos -> Int
toHallway (Hallway _) = 0
toHallway (Room    n) = (n `div` 4) + 1

-- | A vertex in the graph is an array of the positions of all amphipods.
type Burrow = [(Pos, Amphipod)]

canReachHallway :: Burrow -> Pos -> Bool
canReachHallway burrow pos = case pos of
  Hallway _ -> False
  r@(Room n) ->
    null
      . catMaybes
      $ [ lookup (Room i) burrow | i <- [0 .. n - 1], i `mod` 4 == roomIndex r ]

isInFinalPosition :: Int -> Burrow -> Pos -> Bool
isInFinalPosition roomSize burrow pos = case pos of
  Hallway _ -> False
  r@(Room n) ->
    (targetRoomIndex amph == roomIndex r)
      && ( not
         . any (\pos' -> lookup pos' burrow `notElem` [Nothing, Just amph])
         $ [ Room i
           | i <- [n + 1 .. roomSize * 4 - 1]
           , i `mod` 4 == roomIndex r
           ]
         )
  where amph = fromJust $ lookup pos burrow

isBurrowDone :: Int -> Burrow -> Bool
isBurrowDone roomSize burrow =
  all (isInFinalPosition roomSize burrow . fst) burrow

type Vertex = Burrow

data Edge = Edge
  { from   :: Vertex
  , to     :: Vertex
  , weight :: Int
  }
  deriving (Eq, Ord, Show)

-- Before lookup table optimization:
-- Example input: ~11 s
-- Real input: 03:40 s
-- After lookup table optimization:
-- Example input: ~1 s
-- Real input: ~7.5 s.
-- These timings are with profiling turned on.
part1 :: FilePath -> IO Int
part1 filename = do
  burrow <- parse parseBurrow =<< TIO.readFile filename
  let (burrow', dists, _prev) =
        dijkstra burrow (adjacentEdges 2) (isBurrowDone 2)
  pure . fromMaybe 0 $ lookup burrow' dists

part2 :: FilePath -> IO Int
part2 filename = do
  ls <- T.lines <$> TIO.readFile filename
  let burrowData = take 3 ls <> ("#D#C#B#A#" : "#D#B#A#C#" : drop 3 ls)
  burrow <- parse parseBurrow $ T.unlines burrowData
  let (burrow', dists, _prev) =
        dijkstra burrow (adjacentEdges 4) (isBurrowDone 4)
  -- let path = unfoldr (fmap (\s -> (s, s)) . \v -> lookup v prev) burrow'
  -- forM_ (reverse path) $ \v -> do
  --   putStrLn $ showBurrow 4 v
  -- putStrLn $ showBurrow 4 burrow'
  pure . fromMaybe 0 $ lookup burrow' dists

parseBurrow :: Parser Burrow
parseBurrow = do
  mySpace
  xs <- some $ L.lexeme mySpace amphipod
  let xs' = zip (map Room [0 ..]) xs
  pure xs'
  where
    mySpace = void . many $ oneOf (" #.\n" :: String)
    amphipod =
      (void (char 'A') $> A)
        <|> (void (char 'B') $> B)
        <|> (void (char 'C') $> C)
        <|> (void (char 'D') $> D)

-- | Get edges adjacent to a vertex in the graph (i.e. a `Burrow`).
adjacentEdges :: Int -> Vertex -> [Edge]
adjacentEdges roomSize v = foldMap
  (\a@(pos, amph) ->
    map
        (\newPos -> Edge { from   = v
                         , to     = moveAmphipod v a (newPos, amph)
                         , weight = energy amph * distance pos newPos
                         }
        )
      $ nextPositions roomSize v pos
  )
  v

nextPositions :: Int -> Burrow -> Pos -> [Pos]
nextPositions roomSize burrow pos
  | isInRoom pos
    && (  not (canReachHallway burrow pos)
       || isInFinalPosition roomSize burrow pos
       )
  = []
  | not (isInRoom pos)
    && (targetRoomIndex amph `elem` map roomIndex validRoomPositions)
    && not targetRoomContainsWrongAmph
  = validRoomPositions \\ (takenPositions <> sameRoomPositions)
  | isInRoom pos
  = reachableHallwayPositions
    <> (if targetRoomContainsWrongAmph then [] else validRoomPositions)
    \\ (takenPositions <> sameRoomPositions <> invalidHallwayPositions)
  | otherwise
  = []
  where
    hallwayObstacles = mapMaybe
      (\(pos', _) -> case (pos', pos) of
        (Hallway n, Hallway n') -> if n == n' then Nothing else Just n
        (Hallway n, _) -> Just n
        _ -> Nothing
      )
      burrow
    reachableHallwayCoords =
      fromMaybe
          (error $ "LUT lookup failed for: " <> show
            (hallwayObstacles, nearestHallwayCoord pos)
          )
        $  floodFillLUT
        !? (hallwayObstacles, nearestHallwayCoord pos)
    reachableHallwayPositions = map Hallway reachableHallwayCoords
    amph           = snd . fromJust . find ((== pos) . fst) $ burrow
    inRightRoom    = isInRoom pos && fromEnum amph == roomIndex pos
    takenPositions = map fst burrow
    allRooms       = rooms roomSize
    targetRoomContainsWrongAmph =
      let ri = targetRoomIndex amph
      in  not
            . all (\i -> lookup (Room i) burrow `elem` [Nothing, Just amph])
            $ [ri, ri + 4 .. roomSize * 4 - 1]
    validRoomPositions
      | inRightRoom
      = []
      | otherwise
      = ( allRooms
        & filter (\r -> nearestHallwayCoord r `elem` reachableHallwayCoords)
        & filter (\r -> roomIndex r == targetRoomIndex amph)
        )
        \\ takenPositions
        &  sortBy (compare `on` roomIndex)
        &  groupBy ((==) `on` roomIndex)
        &  mapMaybe
             (\xs -> if length xs >= 2 then Just (last xs) else Just (head xs))
    sameRoomPositions = case pos of
      Hallway _ -> []
      Room    n -> filter ((== (n `mod` 4)) . roomIndex) allRooms

moveAmphipod :: Burrow -> (Pos, Amphipod) -> (Pos, Amphipod) -> Burrow
moveAmphipod burrow a a' = DLO.insertBag a' (burrow `DLO.minus` [a])

distance :: Pos -> Pos -> Int
distance p1 p2 = case (min p1 p2, max p1 p2) of
  (Hallway x, Hallway y) -> abs (x - y)
  (Hallway x, r        ) -> toHallway r + abs (nearestHallwayCoord r - x)
  (r        , s        ) -> toHallway r + toHallway s + abs
    (nearestHallwayCoord r - nearestHallwayCoord s)

floodFill :: [Int] -> Int -> [Int]
floodFill xs x = [inf + 1 .. sup - 1]
  where
    (ls, hs) = sort (-1 : 11 : xs) & span (< x)
    inf      = lastDef 0 ls
    sup      = headDef 10 hs

floodFillLUT :: Map ([Int], Int) [Int]
floodFillLUT = foldr
  (\(k, p) acc ->
    let bp = bitPositions (k :: Int) in Map.insert (bp, p) (floodFill bp p) acc
  )
  Map.empty
  [ (k, p) | k <- [0 .. 127], p <- [0 .. 10] ]

-- | Calculate a list of hallway coords (of which there are 7) from a bit mask.
-- Note that this function calls `toHallwayCoord` so that invalid positions are
-- disallowed.
bitPositions :: (Bits a, Num a) => a -> [Int]
bitPositions n =
  map toHallwayCoord
    . catMaybes
    . unfoldr
        (\i -> if i == 7
          then Nothing
          else Just
            (if n .&. (1 `shift` i) /= 0 then Just i else Nothing, i + 1)
        )
    $ 0

-- | Transform a bit index (0 - 6) to a hallway coordinate (0 - 10).
toHallwayCoord :: (Ord a, Num a) => a -> a
toHallwayCoord n = n + min (max 0 (n - 1)) 4

dijkstra
  :: Vertex -- ^ source vertex
  -> (Vertex -> [Edge]) -- ^ function to find adjacent vertices
  -> (Vertex -> Bool) -- ^ predicate to tell if we've arrived at destination
  -> (Vertex, [(Vertex, Int)], [(Vertex, Vertex)])
dijkstra src getAdj isDone = runST $ do
  minDists  <- HST.new
  prev      <- HST.new
  endVertex <- newSTRef []
  let
    go q = case Set.minView q of
      Nothing -> pure ()
      Just ((dist, u), q') ->
        let
          relax qAcc Edge { to = v, ..} = do
            let distViaU = dist + weight
            oldDist <- fromMaybe maxBound <$> HST.lookup minDists v
            if distViaU >= oldDist
              then pure qAcc
              else do
                HST.insert minDists v distViaU
                HST.insert prev v u
                pure . Set.insert (distViaU, v) . Set.delete (oldDist, v) $ qAcc
        in  if isDone u
              then do
                writeSTRef endVertex u
                pure ()
              else foldM relax q' (getAdj u) >>= go
  go (Set.singleton (0, src))
  (,,) <$> readSTRef endVertex <*> H.toList minDists <*> H.toList prev

showBurrow :: Int -> Burrow -> String
showBurrow roomSize burrow =
  mconcat $ (top : [hallwayStr]) <> (roomStr <> [bottom])
  where
    hallwayStr :: String
    hallwayStr =
      '#'
        :  map
             (\n -> case lookup (Hallway n) burrow of
               Nothing   -> '.'
               Just amph -> head . show $ amph
             )
             [0 .. 10]
        <> "#\n"
    roomStr =
      map (\n -> pad n <> roomLine n <> pad n <> "\n") [0 .. roomSize - 1]
    pad n = if n == 0 then "##" else "  "
    top    = "#############\n"
    bottom = "  #########\n"
    roomLine :: Int -> String
    roomLine n = '#' : interleave
      (map
        (\m -> case lookup (Room $ n * 4 + m) burrow of
          Nothing   -> '.'
          Just amph -> head . show $ amph
        )
        [0 .. 3]
      )
      "####"
