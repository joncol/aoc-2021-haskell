module Lib where

import Control.Monad.IO.Class (MonadIO)
import Data.Char (digitToInt)
import Data.Maybe (listToMaybe)
import Data.Text (Text)
import Numeric (readInt)
import Text.Pretty.Simple (CheckColorTty(..), OutputOptions(..),
                           defaultOutputOptionsDarkBg, pPrintOpt, pShowOpt)

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as TL

pPrint :: forall m a . (MonadIO m, Show a) => a -> m ()
pPrint = pPrintOpt
  CheckColorTty
  defaultOutputOptionsDarkBg { outputOptionsCompact   = True
                             , outputOptionsPageWidth = 180
                             }

pShow :: Show a => a -> TL.Text
pShow = pShowOpt
  (defaultOutputOptionsDarkBg { outputOptionsCompact   = True
                              , outputOptionsPageWidth = 180
                              }
  )

readInts :: FilePath -> IO [Int]
readInts filename = map read . lines <$> readFile filename

readStrings :: FilePath -> IO [String]
readStrings filename = filter (not . null) . lines <$> readFile filename

readTexts :: FilePath -> IO [Text]
readTexts filename = filter (not . T.null) . T.lines <$> TIO.readFile filename

transpose :: [[a]] -> [[a]]
transpose = foldr (zipWith (:)) (repeat [])

interleave :: [a] -> [a] -> [a]
interleave xs ys = concat (transpose [xs, ys])

readBin :: Integral a => String -> Maybe a
readBin =
  fmap fst . listToMaybe . readInt 2 (`elem` ("01" :: String)) digitToInt

cap :: Int -> Int -> Int -> Int
cap minVal maxVal val = minVal + ((val - minVal) `myMod` (maxVal - minVal + 1))
  where n `myMod` m = (m + n `mod` m) `mod` m
