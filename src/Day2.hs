{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}

module Day2
  ( part1
  , part2
  ) where

import Data.List (foldl')

data Dir = Forward | Down | Up deriving stock (Eq, Ord, Show)

data Move = Move Dir Int
  deriving stock (Eq, Ord, Show)

parseLines :: FilePath -> IO [Move]
parseLines filename = do
  contents <- readFile filename
  pure . map readMove $ lines contents
  where
    readDir :: String -> Dir
    readDir s | s == "forward" = Forward
              | s == "down"    = Down
              | s == "up"      = Up
              | otherwise      = undefined

    readInt :: String -> Int
    readInt = read

    readMove :: String -> Move
    readMove line = Move dir amount
      where
        [dirStr, amountStr] = words line
        dir                 = readDir dirStr
        amount              = readInt amountStr

part1 :: FilePath -> IO Int
part1 filename = do
  moves <- parseLines filename
  let (x, y) = foldl' move (0, 0) moves
  pure $ x * y
  where
    move :: (Int, Int) -> Move -> (Int, Int)
    move (x, y) m = case m of
      Move Forward n -> (x + n, y)
      Move Down    n -> (x, y + n)
      Move Up      n -> (x, y - n)

part2 :: FilePath -> IO Int
part2 filename = do
  moves <- parseLines filename
  let (x, y, _) = foldl' move (0, 0, 0) moves
  pure $ x * y
  where
    move :: (Int, Int, Int) -> Move -> (Int, Int, Int)
    move (x, y, aim) m = case m of
      Move Forward n -> (x + n, y + aim * n, aim)
      Move Down    n -> (x, y, aim + n)
      Move Up      n -> (x, y, aim - n)
