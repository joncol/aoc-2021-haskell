{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}

module Day14 where

import Data.List (sort, nub)
import Data.Map (Map)
import Text.Megaparsec (count, some)
import Text.Megaparsec.Char (string, upperChar)

import qualified Data.Map as Map
import qualified Data.Text.IO as TIO

import Lib
import Parser
import Data.Maybe (fromJust)

part1 :: FilePath -> IO Int
part1 filename = do
  (template, rules) <- parse parsePolymerData =<< TIO.readFile filename
  let ruleMap = Map.fromList rules
  let fs = map snd . freqs $ iterate (evolve ruleMap) template !! 10
  pure $ maximum fs - minimum fs

part2 :: FilePath -> IO Int
part2 filename = do
  (template, rules) <- parse parsePolymerData =<< TIO.readFile filename
  let initialFreqs = freqs' template

  let ruleMap      = Map.fromList rules
  let pairMap = Map.fromListWith (+) $ zip (pairs template) (repeat 1)
  let (_pairMap', finalFreqs) =
        iterate (evolve' ruleMap) (pairMap, initialFreqs) !! 40

  -- pPrint pairMap
  -- pPrint pairMap'
  -- pPrint finalFreqs

  let vals = Map.elems finalFreqs

  pure $ maximum vals - minimum vals

parsePolymerData :: Parser (String, [(String, Char)])
parsePolymerData = (,) <$> lexeme (some upperChar) <*> some (lexeme rule)
  where
    rule :: Parser (String, Char)
    rule = (,) <$> (count 2 upperChar <* string " -> ") <*> upperChar

evolve :: Map String Char -> String -> String
evolve rules xs = interleave xs toInsert ++ [last xs]
  where toInsert = map (\p -> Map.findWithDefault '?' p rules) $ pairs xs

evolve'
  :: Map String Char
  -> (Map String Int, Map Char Int)
  -> (Map String Int, Map Char Int)
evolve' rules (pairMap, fs) = --(pairMap', fs')
                              (Map.foldrWithKey fRem pairMap' pairMap, fs')
  where
    pairMap' = Map.foldrWithKey fIns pairMap pairMap

    fIns :: String -> Int -> Map String Int -> Map String Int
    fIns pairKey@[k1, k2] oldPairCount m =
      let ch      = fromJust $ Map.lookup pairKey rules
          newKeys = [[k1, ch], [ch, k2]]
      in  foldr
            (Map.alter
              (\case
                Just x  -> Just $ x + oldPairCount
                Nothing -> Just oldPairCount
              )
            )
            m
            newKeys

    fRem k v =
      Map.update (\x -> if x - v == 0 then Nothing else Just $ x - v) k

    fs' :: Map Char Int
    fs' = Map.foldrWithKey fIns' fs pairMap

    fIns' :: String -> Int -> Map Char Int -> Map Char Int
    fIns' pairKey pairFreq m =
      let ch = fromJust $ Map.lookup pairKey rules
      in  Map.alter
            (\case
              Just x  -> Just $ x + pairFreq
              Nothing -> Just pairFreq
            )
            ch
            m

-- evolve'
--   :: Map String Char
--   -> (Map String Int, Map Char Int)
--   -> (Map String Int, Map Char Int)
-- evolve' rules (pairMap, fs) = (Map.foldrWithKey fRem pairMap' pairMap, fs')
--   where
--     pairMap' = Map.foldrWithKey fIns pairMap pairMap

--     fIns k@[k1, k2] v m =
--       let ch      = fromJust $ Map.lookup k rules
--           newKeys = [[k1, ch], [ch, k2]]
--       in  foldr
--             (\nk m' -> Map.insertWith (\nv ov -> v + ov * nv)
--                                       nk
--                                       (Map.findWithDefault 1 nk pairMap)
--                                       m'
--             )
--             m
--             newKeys

--     fRem k v m =
--       Map.update (\x -> if x - v == 0 then Nothing else Just $ x - v) k m

--     fs' :: Map Char Int
--     fs' = Map.foldrWithKey fIns' fs pairMap

--     fIns' :: String -> Int -> Map Char Int -> Map Char Int
--     fIns' pairKey pairFreq m =
--       let ch = fromJust $ Map.lookup pairKey rules
--       in  Map.insertWith (\nv ov -> 1 + ov * nv) ch pairFreq m

pairs :: [a] -> [[a]]
pairs []           = []
pairs [_         ] = []
pairs (x : y : ys) = [x, y] : pairs (y : ys)

freqs :: String -> [(Char, Int)]
freqs xs =
  [ (x, n) | x <- nub . sort $ xs, let n = length . filter (== x) $ xs, n > 0 ]

freqs' :: String -> Map Char Int
freqs' xs = Map.fromList
  [ (x, n) | x <- nub . sort $ xs, let n = length . filter (== x) $ xs, n > 0 ]
