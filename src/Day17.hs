module Day17 where

import Control.Monad (void)
import Text.Megaparsec.Char (string)

import qualified Data.Text.IO as TIO

import Parser

type Pos = (Int, Int)
type Speed = (Int, Int)

part1 :: FilePath -> IO Int
part1 filename = do
  ((_, y1), (_, y2)) <- parse parseTargetArea =<< TIO.readFile filename
  let ys   = take 100 $ map (\vy -> (vy, hitsTargetY y1 y2 vy)) [1 ..]
  let vmax = fst . head . dropWhile ((== False) . snd) . reverse $ ys

  pure $ vmax * (vmax + 1) `div` 2

part2 :: FilePath -> IO Int
part2 filename = do
  target@((x1, y1), (x2, y2)) <- parse parseTargetArea =<< TIO.readFile filename

  let ys   = [-80 .. 80]
  let hy   = filter (hitsTargetY y1 y2) ys

  let xs   = [1 .. 314]
  let hx   = filter (hitsTargetX x1 x2) xs

  let hits = [ (vx, vy) | vx <- hx, vy <- hy, hitsTarget target (vx, vy) ]
  pure $ length hits

hitsTargetX :: Int -> Int -> Int -> Bool
hitsTargetX x1 x2 v0x =
  (v0x * (v0x + 1) `div` 2 >= x1)
    && ( (<= x2)
       . fst
       . fst
       . head
       . dropWhile (\((px, _), _) -> px < x1)
       . iterate move
       $ ((0, 0), (v0x, 0))
       )

hitsTargetY :: Int -> Int -> Int -> Bool
hitsTargetY y1 y2 v0y =
  (>= y1)
    . snd
    . fst
    . head
    . dropWhile (\((_, py), _) -> py > y2)
    . iterate move
    $ ((0, 0), (0, v0y))

hitsTarget :: ((Int, Int), (Int, Int)) -> Speed -> Bool
hitsTarget ((x1, y1), (x2, y2)) v0 =
  any (\((x, y), _) -> x1 <= x && x <= x2 && y1 <= y && y <= y2)
    . take 170
    . iterate move
    $ ((0, 0), v0)

parseTargetArea :: Parser (Pos, Pos)
parseTargetArea = do
  void $ string "target area: x="
  x1 <- signedInteger
  void $ string ".."
  x2 <- signedInteger
  void $ string ", y="
  y1 <- signedInteger
  void $ string ".."
  y2 <- signedInteger
  pure ((x1, y1), (x2, y2))

move :: (Pos, Speed) -> (Pos, Speed)
move ((px, py), (vx, vy)) = ((px + vx, py + vy), (vx', vy - 1))
  where
    vx' = if
      | vx > 0    -> vx - 1
      | vx < 0    -> vx + 1
      | otherwise -> 0
