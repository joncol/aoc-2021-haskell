{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Day24 where

import Control.Arrow ((&&&), (>>>))
import Control.DeepSeq (NFData)
import Control.Exception (ArithException (..), throw)
import Control.Monad.State
import Data.Cache.LRU (LRU)
import Data.Foldable (traverse_)
import Data.Functor (($>))
import Data.List (find, inits, scanl')
import Data.Map (Map, (!?))
import Data.Maybe (fromJust, mapMaybe)
import GHC.Generics (Generic)
import Optics
import Safe (headMay)
import Text.Megaparsec hiding (State, parse)
import Text.Megaparsec.Char

import qualified Data.Cache.LRU as LRU
import qualified Data.Map as Map
import qualified Data.Text.IO as TIO
import qualified Text.PrettyPrint.Leijen as PP

import Parser
import Lib

data Instruction =
    Inp Register
  | Add Register RegisterOrValue
  | Mul Register RegisterOrValue
  | Div Register RegisterOrValue
  | Mod Register RegisterOrValue
  | Eql Register RegisterOrValue
  deriving (Eq, Ord, Show)

newtype InstructionPP = InstructionPP Instruction

instance PP.Pretty InstructionPP where
  pretty (InstructionPP (Inp reg)) =
    PP.text $ show reg <> " = " <> "int(inp.popleft())"
  pretty (InstructionPP (Add reg regOrVal)) =
    PP.text $ show reg <> " += " <> show regOrVal
  pretty (InstructionPP (Mul reg regOrVal)) =
    PP.text $ show reg <> " *= " <> show regOrVal
  pretty (InstructionPP (Div reg regOrVal)) =
    PP.text $ show reg <> " //= " <> show regOrVal
  pretty (InstructionPP (Mod reg regOrVal)) =
    PP.text $ show reg <> " %= " <> show regOrVal
  pretty (InstructionPP (Eql reg regOrVal)) =
    PP.text
      $  show reg
      <> " = 1 if "
      <> show reg
      <> " == "
      <> show regOrVal
      <> " else 0"

data Register = X | Y | Z | W deriving (Eq, Ord, Generic, NFData)

instance Show Register where
  show X = "x"
  show Y = "y"
  show Z = "z"
  show W = "w"

data RegisterOrValue = Reg Register | Val Int deriving (Eq, Ord)

instance Show RegisterOrValue where
  show (Reg reg) = show reg
  show (Val n  ) = show n

data ALU = ALU
  { regs         :: Map Register Int
  , inp          :: String
  , pc           :: Int -- program counter
  , currentInpCh :: Maybe Char
  , consumedInp  :: String
  , cache        :: Cache
  }
  deriving (Eq, Show, Generic)

type Program = [Instruction]

newtype ProgramPP = ProgramPP Program

instance PP.Pretty ProgramPP where
  pretty (ProgramPP prog) =
    PP.vcat (map PP.text preamble)
      <> (if null prog
           then PP.empty
           else PP.linebreak
             <> PP.indent 4 (PP.vcat (map (PP.pretty . InstructionPP) prog))
         )
      <> PP.linebreak
      <> PP.linebreak
      <> PP.vcat (map PP.text finale)
      <> PP.linebreak
    where
      preamble =
        [ "from collections import deque"
        , "import itertools"
        , "import sys"
        , ""
        , "def calculate_zs(count):"
        , "    all_digits = itertools.repeat(list(range(9, 0, -1)), 14)"
        , "    nums = itertools.product(*all_digits)"
        , "    return map(calculate_z, itertools.islice(nums, count))"
        , ""
        , "def calculate_z(digits):"
        , "    num = ''.join(map(str, digits))"
        , "    inp = deque(num)"
        , "    x = y = z = w = 0"
        , ""
        ]
      finale =
        [ "    return (num, z)"
        , ""
        , "def main():"
        , "    count = int(sys.argv[1]) if len(sys.argv) > 1 else None"
        , "    zs = calculate_zs(count)"
        , "    print(list(itertools.dropwhile(lambda nz: nz[1] != 0, zs))[0])"
        , ""
        , "if __name__ == \"__main__\":"
        , "    main()"
        ]

-- | Stores register values and program counter.
type Cache = LRU String [Int]

part1 :: FilePath -> IO Int
part1 filename = do
  prog <- parse parseProgram =<< TIO.readFile filename

  writeFile "day24/day24.py" . show . PP.pretty . ProgramPP $ prog

  let nums = replicateM 14 (reverse "123456789" :: String)
  let res = scanl'
        (\(_, alu) n -> runState (runProgram prog) $ resetALU n (cache alu))
        (False, resetALU "" (LRU.newLRU $ Just 100000))
        (take 1000 nums)
  putStrLn
    . unlines
    . map ((view #regs >>> Map.lookup Z >>> fromJust >>> show) . snd)
    . tail
    $ res
  -- pure . read . consumedInp . snd . fromJust . find ((== True) . fst) $ res
  pure 0

parseProgram :: Parser [Instruction]
parseProgram = some (lexeme parseInstruction)

parseInstruction :: Parser Instruction
parseInstruction = inp <|> add <|> mul <|> div' <|> mod' <|> eql
  where
    str  = lexeme . string
    chr  = lexeme . char
    inp  = void (str "inp") *> (Inp <$> reg)
    add  = void (str "add") *> (Add <$> reg <*> regOrVal)
    mul  = void (str "mul") *> (Mul <$> reg <*> regOrVal)
    div' = void (str "div") *> (Div <$> reg <*> regOrVal)
    mod' = void (str "mod") *> (Mod <$> reg <*> regOrVal)
    eql  = void (str "eql") *> (Eql <$> reg <*> regOrVal)
    reg =
      (void (chr 'x') $> X)
        <|> (void (chr 'y') $> Y)
        <|> (void (chr 'z') $> Z)
        <|> (void (chr 'w') $> W)
    regOrVal = Reg <$> reg <|> Val <$> signedInteger

resetALU :: String -> Cache -> ALU
resetALU inp cache = ALU { regs = Map.fromList [(X, 0), (Y, 0), (Z, 0), (W, 0)]
                         , inp          = inp
                         , pc           = 0
                         , currentInpCh = Nothing
                         , consumedInp  = ""
                         , cache        = cache
                         }

runInstruction :: Instruction -> State ALU ()
runInstruction (Inp reg) = do
  s <- get
  case currentInpCh s of
    Nothing -> pure ()
    Just ch -> do
      modify
        (over
          #cache
          (LRU.insert (consumedInp s <> [ch]) (Map.elems (regs s) <> [pc s]))
        )
      modify (over #consumedInp (<> [ch]))
  let inpCh = head . inp $ s
  let inp   = read . (: []) $ inpCh -- convert to `Int`
  modify (set #currentInpCh (Just inpCh) . over #inp tail)
  modify (over #regs $ Map.insert reg inp)
runInstruction (Add reg regOrVal) = binOp reg regOrVal ((pure .) . (+))
runInstruction (Mul reg regOrVal) = binOp reg regOrVal ((pure .) . (*))
runInstruction (Div reg regOrVal) = binOp reg regOrVal ((pure .) . div)
runInstruction (Mod reg regOrVal) = binOp
  reg
  regOrVal
  (\a b -> do
    when (a < 0) $ throw Underflow
    when (b == 0) $ throw DivideByZero
    when (b < 0) $ throw Underflow
    pure $ a `mod` b
  )
runInstruction (Eql reg regOrVal) =
  binOp reg regOrVal (\x y -> pure $ if x == y then 1 else 0)

binOp
  :: (MonadState ALU m)
  => Register
  -> RegisterOrValue
  -> (forall m1 . Monad m1 => Int -> Int -> m1 Int)
  -> m ()
binOp a b f = do
  b' <- case b of
    Reg reg' -> gets (fromJust . Map.lookup reg' . regs)
    Val val  -> pure val
  modify (over #regs $ Map.update (Just <=< flip f b') a)

runProgram :: Program -> State ALU Bool
runProgram = runProgram' True

-- | Returns state value true if the model number provided in `inp` of the
-- initial state is a valid submarine model number.
runProgram' :: Bool -> Program -> State ALU Bool
runProgram' useCache prog = do
  when useCache $ do
    (cache, inp) <- gets (cache &&& inp)
    let ks = reverse . tail . inits . init $ inp
    case
        headMay
        . mapMaybe
            (\k ->
              (k, )
                <$> let (cache', mval) = LRU.lookup k cache
                    in  mval >>= (\val -> Just (cache', val))
            )
        $ ks
      of
        Nothing -> pure ()
        Just (k, (cache', [x, y, z, w, pc])) -> do
          modify
            ( set #regs (Map.fromList [(X, x), (Y, y), (Z, z), (W, w)])
            . set #pc          pc
            . set #consumedInp k
            . over #inp (drop $ length k)
            . set #cache cache'
            )
          -- s <- get
          -- error $ "cache hit for '" <> k <> "', state: " <> T.unpack (pShow s)
        Just _ -> error "invalid cache contents"

  pc <- gets pc
  traverse_
      (\i -> do
        runInstruction i
        modify (over #pc (+ 1))
      )
    $ drop pc prog

  s <- get
  maybe (pure ()) (\ch -> modify (over #consumedInp (<> [ch]))) (currentInpCh s)

  let z = fromJust . Map.lookup Z $ regs s
 -- pure $ (trace ("ALU: " <> show s) z) == 0
  pure $ z == 0
