module Day25 where

import Control.Monad (forM_)
import Control.Monad.ST (ST, runST)
import Data.Array
import Data.Array.ST
import Data.List (elemIndex, intercalate)
import Data.List.Split (chunksOf)
import Data.Maybe (isNothing, fromJust)
import Lib (transpose)

type Seafloor = Array Pos (Maybe SeaCucumber)
type Pos = (Int, Int)
data SeaCucumber = EastFacing | SouthFacing deriving (Eq, Ord, Show)

part1 :: FilePath -> IO Int
part1 filename = do
  ls <- lines <$> readFile filename
  let m  = parseSeafloor ls
  let xs = iterate step m
  let zs = zipWith (==) xs (tail xs)
  pure . fromJust $ (+ 1) <$> (True `elemIndex` zs)

parseSeafloor :: [String] -> Seafloor
parseSeafloor ls = listArray
  ((0, 0), (w - 1, h - 1))
  [ atPos x y | x <- [0 .. w - 1], y <- [0 .. h - 1] ]
  where
    w = length . head $ ls
    h = length ls

    atPos :: Int -> Int -> Maybe SeaCucumber
    atPos x y = case (ls !! y) !! x of
      '.' -> Nothing
      '>' -> Just EastFacing
      'v' -> Just SouthFacing
      _   -> error "invalid seafloor map character"

step :: Seafloor -> Seafloor
step m = runST $ do
  m' <- thawST m
  let move from to =
        readArray m' from >>= writeArray m' to >> writeArray m' from Nothing
  forM_ [ (x, y) | x <- [x1 .. x2], y <- [y1 .. y2] ] $ \(x, y) -> do
    if (m ! (x, y) == Just EastFacing) && isNothing (m ! ((x + 1) `mod` w, y))
      then move (x, y) ((x + 1) `mod` w, y)
      else pure ()
  t <- freeze m'
  forM_ [ (x, y) | x <- [x1 .. x2], y <- [y1 .. y2] ] $ \(x, y) -> do
    if (t ! (x, y) == Just SouthFacing) && isNothing (t ! (x, (y + 1) `mod` h))
      then move (x, y) (x, (y + 1) `mod` h)
      else pure ()
  freeze m'
  where
    thawST :: Ix i => Array i e -> ST s (STArray s i e)
    thawST               = thaw
    ((x1, y1), (x2, y2)) = bounds m
    w                    = x2 - x1 + 1
    h                    = y2 - y1 + 1

showSeafloor :: Seafloor -> String
showSeafloor m =
  intercalate "\n" . map (map toChar) . transpose . chunksOf h . elems $ m
  where
    ((_, y1), (_, y2)) = bounds m
    h                  = y2 - y1 + 1
    toChar x = case x of
      Nothing          -> '.'
      Just EastFacing  -> '>'
      Just SouthFacing -> 'v'
